package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud;
import com.atlassian.bitbucket.linky.discovery.CloudBitbucketRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.discovery.RemoteUrl;
import com.atlassian.bitbucket.linky.discovery.ServerBitbucketRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.RepositoriesKt;
import com.atlassian.bitbucket.server.rest.BitbucketServer;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.ListCellRendererWrapper;
import com.intellij.ui.components.JBTextField;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LinkConfigurationForm implements Configurable {
    private final CloudBitbucketRepositoryDiscoverer cloudDiscoverer;
    private final ServerBitbucketRepositoryDiscoverer serverDiscoverer;

    private JPanel mainPanel;
    private ComboBox<RemoteUrl> remoteComboBox;
    private JBTextField repoUrlTextField;
    private JTextField pathTextField;

    LinkConfigurationForm(Repository repository) {
        Project project = repository.getProject();
        this.cloudDiscoverer = ServiceManager.getService(project, CloudBitbucketRepositoryDiscoverer.class);
        this.serverDiscoverer = ServiceManager.getService(project, ServerBitbucketRepositoryDiscoverer.class);

        String relativePath = VfsUtil.getRelativePath(repository.getRoot(), repository.getProject().getBaseDir());
        if (StringUtils.isBlank(relativePath)) {
            relativePath = "<Project Root>";
        }
        pathTextField.setText(relativePath);

        List<RemoteUrl> remoteUrls = RepositoriesKt.getRemoteUrls(repository);
        remoteComboBox.setModel(new CollectionComboBoxModel<>(remoteUrls));
        if (remoteUrls.size() <= 1) {
            remoteComboBox.setEnabled(false);
        }
    }

    @Override
    public void disposeUIResources() {
    }

    @Override
    @NotNull
    public JComponent createComponent() {
        return mainPanel;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Repository Link Configuration";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public boolean isModified() {
        return getRepositoryUrl()
                .map(url -> getBitbucketCloud(url).isPresent() || getBitbucketServer(url).isPresent())
                .orElse(false);
    }

    @Override
    public void reset() {
        if (remoteComboBox.getItemCount() > 0) {
            remoteComboBox.setSelectedIndex(0);
        }
        repoUrlTextField.setText("");
    }

    @Override
    public void apply() {
        Optional.ofNullable((RemoteUrl) remoteComboBox.getSelectedItem())
                .ifPresent(remoteUrl ->
                        getRepositoryUrl()
                                .ifPresent(url -> {
                                    Optional<BitbucketCloud> maybeCloud = getBitbucketCloud(url);
                                    if (maybeCloud.isPresent()) {
                                        maybeCloud.ifPresent(cloud ->
                                                cloudDiscoverer.registerManuallyConfigured(remoteUrl, cloud));
                                    } else {
                                        Optional<BitbucketServer> maybeServer = getBitbucketServer(url);
                                        maybeServer.ifPresent(server ->
                                                serverDiscoverer.registerManuallyConfigured(remoteUrl, server));
                                    }
                                }));
    }

    private Optional<URI> getRepositoryUrl() {
        try {
            return Optional.of(URI.create(repoUrlTextField.getText()));
        } catch (IllegalArgumentException ignored) {
            return Optional.empty();
        }
    }

    private Optional<BitbucketCloud> getBitbucketCloud(URI repoUrl) {
        try {
            return Optional.of(BitbucketCloud.Companion.forUrl(repoUrl));
        } catch (IllegalArgumentException ignored) {
            return Optional.empty();
        }
    }

    private Optional<BitbucketServer> getBitbucketServer(URI repoUrl) {
        List<String> pathSegments = Arrays.asList(repoUrl.getPath().split("/"));
        // BBS URL path should be .../projects/KEY/repos/SLUG/...
        int projectsIndex = pathSegments.lastIndexOf("projects");
        int reposIndex = pathSegments.lastIndexOf("repos");
        if (reposIndex == projectsIndex + 2) {
            URI baseUrl = repoUrl.resolve("/" + String.join("/", pathSegments.subList(0, projectsIndex)));
            return Optional.of(new BitbucketServer(baseUrl));
        } else {
            return Optional.empty();
        }
    }

    private void createUIComponents() {
        remoteComboBox = new ComboBox<>();
        remoteComboBox.setRenderer(new ListCellRendererWrapper<RemoteUrl>() {
            @Override
            public void customize(JList list, RemoteUrl remoteUrl, int index, boolean selected, boolean hasFocus) {
                StringBuilder sb = new StringBuilder();
                sb.append(remoteUrl.getScheme()).append("://").append(remoteUrl.getHostname());
                if (remoteUrl.getPort() > 0) {
                    sb.append(":").append(remoteUrl.getPort());
                }
                sb.append(remoteUrl.getPath());
                setText(sb.toString());
            }
        });

        repoUrlTextField = new JBTextField();
        repoUrlTextField.getEmptyText().setText("Paste a Bitbucket URL for this repository");
    }
}
