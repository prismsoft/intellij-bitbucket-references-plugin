package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService;
import com.atlassian.bitbucket.linky.discovery.RemoteUrl;
import com.atlassian.bitbucket.linky.repository.RepositoriesKt;
import com.atlassian.bitbucket.linky.repository.RepositorySettings;
import com.atlassian.bitbucket.linky.repository.SettingKey;
import com.intellij.dvcs.repo.Repository;
import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.ListCellRendererWrapper;
import com.intellij.ui.components.JBTextField;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static io.ktor.http.URLUtilsJvmKt.toURI;

public class LinkedRepositoryConfigurationForm implements Configurable {
    private final TwoStepRepositorySettings settings;
    private final Repository repository;
    private final BitbucketRepositoriesService bitbucketRepositoriesService;

    private JBTextField branchNameTextField;
    private JPanel mainPanel;
    private JLabel urlLabel;
    private JRadioButton automaticRadioButton;
    private JRadioButton fixedRadioButton;
    private ComboBox bbReposComboBox;

    private BitbucketRepository currentBitbucketRepository;

    private AtomicBoolean updatingState = new AtomicBoolean(false);

    LinkedRepositoryConfigurationForm(RepositorySettings settings,
                                      Repository repository,
                                      BitbucketRepositoriesService bitbucketRepositoriesService) {
        this.settings = new TwoStepRepositorySettings(settings);
        this.repository = repository;
        this.bitbucketRepositoriesService = bitbucketRepositoriesService;

        List<BitbucketRepository> bbRepos = new ArrayList<>(bitbucketRepositoriesService.getBitbucketRepositories(repository).values());
        bbReposComboBox.setModel(new CollectionComboBoxModel<>(bbRepos));

        automaticRadioButton.addActionListener(event -> remoteSelectionChanged());
        fixedRadioButton.addActionListener(event -> remoteSelectionChanged());
    }

    @Override
    public void disposeUIResources() {
    }

    @NotNull
    @Override
    public JComponent createComponent() {
        return mainPanel;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Repository Configuration";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public boolean isModified() {
        return settings.isModified();
    }

    @Override
    public void apply() throws ConfigurationException {
        settings.apply();
    }

    @Override
    public void reset() {
        updatingState.set(true);

        settings.reset();
        branchNameTextField.setText(settings.getProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME, ""));

        Optional<RemoteUrl> fixedRemoteUrl = getFixedRemoteUrl();
        if (fixedRemoteUrl.isPresent()) {
            BitbucketRepository bbRepo = bitbucketRepositoriesService.getBitbucketRepositories(repository).get(fixedRemoteUrl.get());
            bbReposComboBox.setSelectedItem(bbRepo);
            fixedBitbucketRepositoryEnabled(bbRepo);
        } else {
            automaticSelectionEnabled();
        }

        updatingState.set(false);
    }

    private Optional<RemoteUrl> getFixedRemoteUrl() {
        return Optional.ofNullable(settings.getProperty(repository, SettingKey.ALWAYS_LINK_TO_SELECTED_REMOTE, null))
                .flatMap(fixedRemote -> Optional.ofNullable(RemoteUrl.Companion.parse(fixedRemote)));
    }

    private void createUIComponents() {
        urlLabel = new JLabel();
        urlLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        urlLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                BrowserUtil.browse(toURI(currentBitbucketRepository.getBaseUri()));
            }
        });

        bbReposComboBox = new ComboBox();
        bbReposComboBox.setRenderer(new ListCellRendererWrapper<BitbucketRepository>() {
            @Override
            public void customize(JList list, BitbucketRepository bbRepo, int index, boolean selected, boolean hasFocus) {
                setText(String.format("<html><b>%s:</b> %s</html>", bbRepo.getClass().getSimpleName(), bbRepo.getFullSlug()));
            }
        });
        bbReposComboBox.addActionListener(e -> remoteSelectionChanged());

        branchNameTextField = new JBTextField();
        branchNameTextField.getEmptyText().setText("<default branch>");
        branchNameTextField.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            protected void textChanged(DocumentEvent documentEvent) {
                String userInput = branchNameTextField.getText();
                String oldBranchName = settings.getProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME, "");
                if (!StringUtils.equals(userInput, oldBranchName)) {
                    if (StringUtils.isBlank(userInput)) {
                        settings.removeProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME);
                    } else {
                        settings.setProperty(repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME, userInput);
                    }
                }
            }
        });
    }

    private void remoteSelectionChanged() {
        if (updatingState.get()) {
            return;
        }

        if (automaticRadioButton.isSelected()) {
            settings.removeProperty(repository, SettingKey.ALWAYS_LINK_TO_SELECTED_REMOTE);
            automaticSelectionEnabled();
        } else {
            BitbucketRepository bbRepo = (BitbucketRepository) bbReposComboBox.getSelectedItem();
            settings.setProperty(repository, SettingKey.ALWAYS_LINK_TO_SELECTED_REMOTE, bbRepo.getRemoteUrl().serialize());
            fixedBitbucketRepositoryEnabled(bbRepo);
        }
    }

    private void automaticSelectionEnabled() {
        Map<RemoteUrl, BitbucketRepository> bbRepos = bitbucketRepositoriesService.getBitbucketRepositories(repository);
        RepositoriesKt.getOrderedRemoteUrls(repository).stream()
                .map(bbRepos::get)
                .filter(Objects::nonNull)
                .findFirst()
                .ifPresent(bbRepo -> currentBitbucketRepository = bbRepo);
        updateBitbucketUrl();

        automaticRadioButton.setSelected(true);
        fixedRadioButton.setSelected(false);
        bbReposComboBox.setEnabled(false);
    }

    private void fixedBitbucketRepositoryEnabled(BitbucketRepository bbRepo) {
        currentBitbucketRepository = bbRepo;
        updateBitbucketUrl();

        automaticRadioButton.setSelected(false);
        fixedRadioButton.setSelected(true);
        bbReposComboBox.setEnabled(true);
    }

    private void updateBitbucketUrl() {
        urlLabel.setText("<html><a href=\"#\">" + currentBitbucketRepository.getBaseUri().toString() + "</a></html>");
    }
}
