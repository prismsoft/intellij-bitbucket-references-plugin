package com.atlassian.bitbucket.linky.stats

import com.atlassian.bitbucket.linky.coroutines.IO
import com.atlassian.bitbucket.linky.utils.createHttpClient
import com.atlassian.bitbucket.util.submitForm
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import com.intellij.openapi.application.Application
import io.ktor.client.response.HttpResponse
import io.ktor.http.Parameters
import io.ktor.http.Url
import io.ktor.http.isSuccess
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext
import java.io.IOException
import java.time.Instant
import kotlin.coroutines.CoroutineContext

data class AmplitudeEvent(val deviceId: String,
                          val eventType: String,
                          val time: Instant,
                          val platform: String,
                          val appVersion: String,
                          val osName: String,
                          val osVersion: String,
                          val eventProperties: Map<String, String> = mapOf(),
                          val userProperties: Map<String, String> = mapOf())

interface AmplitudeClient : CoroutineScope {
    @Throws(AmplitudeException::class)
    suspend fun sendEvents(events: Collection<AmplitudeEvent>)
}

class AmplitudeException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

private val AMPLITUDE_API_URL = Url("https://api.amplitude.com/httpapi")

class DefaultAmplitudeClient(application: Application) : AmplitudeClient {

    private val apiKey = if (application.isInternal) "35c5a9fae64c76956f586bc0f97dc0a8" else "f29e96dfe10d70c847f65062891f7533"
    private val gson = createGson()
    private val httpClient = createHttpClient(AMPLITUDE_API_URL)

    override val coroutineContext: CoroutineContext
        get() = httpClient.coroutineContext

    override suspend fun sendEvents(events: Collection<AmplitudeEvent>) {
        if (events.isEmpty()) return

        val formData = Parameters.build {
            append("api_key", apiKey)
            append("event", events.joinToString(separator = ",", prefix = "[", postfix = "]", transform = { gson.toJson(it) }))
        }

        try {
            val response = withContext(IO) {
                httpClient.submitForm<HttpResponse>(AMPLITUDE_API_URL, formData)
            }
            val status = response.status
            if (!status.isSuccess()) {
                throw AmplitudeException("Got ${status.value} error response from Amplitude: ${status.description}")
            }
        } catch (e: IOException) {
            throw AmplitudeException("Failed to request Amplitude", e)
        }
    }
}

private fun createGson() = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .registerTypeAdapter(Instant::class.java, JsonSerializer<Instant> { instant, _, _ ->
            JsonPrimitive(instant.toEpochMilli())
        })
        .create()
