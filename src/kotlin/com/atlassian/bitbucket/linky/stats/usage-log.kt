package com.atlassian.bitbucket.linky.stats

import com.atlassian.bitbucket.linky.SETTINGS_STORAGE_FILE
import com.atlassian.bitbucket.linky.utils.getInstant
import com.atlassian.bitbucket.linky.utils.getString
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import org.jdom.Element
import java.time.Instant
import java.time.format.DateTimeFormatter.ISO_INSTANT
import java.util.concurrent.ConcurrentLinkedQueue

data class UsageEvent(val event: String,
                      val place: String,
                      val timestamp: Instant,
                      val attributes: Map<String, String>)

interface UsageLog {
    fun event(event: String, place: String, attributes: Map<String, Any> = emptyMap())
}

interface MutableUsageLog {
    fun getEvents(): List<UsageEvent>
    fun removeEvents(eventsToRemove: Collection<UsageEvent>)
}

@State(name = "UsageLog", storages = [Storage(SETTINGS_STORAGE_FILE)])
class UsageLogState : UsageLog, MutableUsageLog, PersistentStateComponent<Element> {
    private val events = ConcurrentLinkedQueue<UsageEvent>()

    override fun event(event: String, place: String, attributes: Map<String, Any>) {
        events.add(UsageEvent(event, place, Instant.now(), attributes.mapValues { it.value.toString() }))
    }

    override fun getEvents(): List<UsageEvent> = events.toList()

    override fun removeEvents(eventsToRemove: Collection<UsageEvent>) {
        events.removeAll(eventsToRemove)
    }

    override fun getState() =
            Element("state").apply {
                events.forEach { event ->
                    addContent(Element("event").apply {
                        setAttribute("name", event.event)
                        setAttribute("place", event.place)
                        setAttribute("timestamp", ISO_INSTANT.format(event.timestamp))
                        event.attributes.forEach { attribute ->
                            addContent(Element("attribute").apply {
                                setAttribute("name", attribute.key)
                                text = attribute.value
                            })
                        }
                    })
                }
            }

    override fun loadState(element: Element) {
        events.clear()
        element.getChildren("event")
                .forEach { e ->
                    val event = e.getString("name")
                    val place = e.getString("place")
                    val timestamp = e.getInstant("timestamp")
                    val attributes = e.getChildren("attribute")
                            .mapNotNull {
                                val name = it.getString("name")
                                val value = it.text
                                when {
                                    name != null && value != null -> name to value
                                    else -> null
                                }
                            }
                            .toMap()
                    if (event != null && place != null && timestamp != null) {
                        events.add(UsageEvent(event, place, timestamp, attributes))
                    }
                }
    }
}

val cloudEventAttributes = mapOf("bitbucket" to "cloud")
val serverEventAttributes = mapOf("bitbucket" to "server")
