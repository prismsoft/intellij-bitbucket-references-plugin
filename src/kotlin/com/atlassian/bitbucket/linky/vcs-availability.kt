package com.atlassian.bitbucket.linky

private fun isClassAvailable(className: String): Boolean {
    try {
        Class.forName(className)
        return true
    } catch (e: ClassNotFoundException) {
        return false
    }
}

fun isGitAvailable() = isClassAvailable("git4idea.commands.Git")

fun isHgAvailable() = isClassAvailable("org.zmlx.hg4idea.HgVcs")
