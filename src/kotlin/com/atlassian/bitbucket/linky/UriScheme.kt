package com.atlassian.bitbucket.linky

import io.ktor.http.URLProtocol

enum class UriScheme constructor(val presentation: String) {
    GIT("git"),
    HTTP("http"),
    HTTPS("https"),
    SSH("ssh");

    override fun toString(): String {
        return presentation
    }

    fun toUrlProtocol() = when (this) {
        GIT, SSH -> URLProtocol(presentation, 22)
        HTTP -> URLProtocol.HTTP
        HTTPS -> URLProtocol.HTTPS
    }

    companion object {
        fun forName(presentation: String?): UriScheme? =
                values().find { it.presentation.equals(presentation, ignoreCase = true) }
    }
}
