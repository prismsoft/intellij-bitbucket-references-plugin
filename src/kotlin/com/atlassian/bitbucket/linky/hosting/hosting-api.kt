@file:JvmName("HostingUtils")

package com.atlassian.bitbucket.linky.hosting

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.server.rest.BitbucketServer
import io.ktor.http.toURI
import java.util.*

val BitbucketCloud.baseUri
    get() = baseUrl.toURI()

val BitbucketCloud.name: String
    get() = this.javaClass.simpleName

val BitbucketServer.uuid: UUID
    get() = UUID.nameUUIDFromBytes(baseUrl.toString().toByteArray())

interface BitbucketCloudRegistry {
    fun add(scheme: UriScheme, hostname: String, port: Int, cloud: BitbucketCloud)
    fun lookup(scheme: UriScheme, hostname: String, port: Int): BitbucketCloud?
    fun isStagingDiscovered(): Boolean
}

interface BitbucketServerRegistry {
    fun add(scheme: UriScheme, hostname: String, port: Int, path: String, server: BitbucketServer)
    fun lookup(scheme: UriScheme, hostname: String, port: Int, path: String): BitbucketServer?
}
