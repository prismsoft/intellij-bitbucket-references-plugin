package com.atlassian.bitbucket.linky.hosting

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.BitbucketCloud.Production
import com.atlassian.bitbucket.cloud.rest.BitbucketCloud.Staging
import com.atlassian.bitbucket.linky.SETTINGS_STORAGE_FILE
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.HTTP
import com.atlassian.bitbucket.linky.utils.*
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import org.jdom.Element
import java.util.function.Function

@State(name = "BitbucketCloudRegistry", storages = [
    Storage("bitbucket-cloud-registry.xml", deprecated = true),
    Storage(SETTINGS_STORAGE_FILE)
])
class DefaultBitbucketCloudRegistry : BitbucketCloudRegistry, PersistentStateComponent<Element> {
    private val lookupMap: MutableMap<BitbucketCloudCoordinates, BitbucketCloud> = mutableMapOf()

    override fun add(scheme: UriScheme, hostname: String, port: Int, cloud: BitbucketCloud) {
        lookupMap.putIfAbsent(BitbucketCloudCoordinates(scheme, hostname, port), cloud)
    }

    override fun lookup(scheme: UriScheme, hostname: String, port: Int) =
            lookupMap[BitbucketCloudCoordinates(scheme, hostname, port)]

    override fun isStagingDiscovered() = lookupMap.values.contains(Staging)

    override fun getState() =
            Element("BitbucketCloudRegistry").apply {
                addContent(Element("lookup").apply {
                    lookupMap.toSortedMap().forEach { e ->
                        addContent(Element("record").apply {
                            setAttribute("scheme", e.key.scheme.toString())
                            setAttribute("hostname", e.key.hostname)
                            // TODO do not save -1
                            setAttribute("port", e.key.port.toString())
                            setAttribute("instance", e.value.name)
                        })
                    }
                })
            }

    override fun loadState(element: Element) {
        lookupMap.clear()
        element.getChild("lookup").getChildren("record")
                .forEach { e ->
                    val uriScheme = e.getUriScheme("scheme")
                    val hostname = e.getString("hostname")
                    val port = e.getInt("port")
                    val instance = when (e.getString("instance")) {
                        Production.name -> Production
                        Staging.name -> Staging
                        else -> null
                    }

                    if (uriScheme != null && hostname != null && port != null && instance != null) {
                        lookupMap[BitbucketCloudCoordinates(uriScheme, hostname, port)] = instance
                    }
                }
    }

    data class BitbucketCloudCoordinates(val scheme: UriScheme = HTTP, val hostname: String, val port: Int) : Comparable<BitbucketCloudCoordinates> {
        override fun compareTo(other: BitbucketCloudCoordinates) =
                Comparator.comparing(Function<BitbucketCloudCoordinates, String> { t -> t.hostname })
                        .thenComparing { t -> t.port }
                        .thenComparing { t -> t.scheme }
                        .compare(this, other)
    }
}

@State(name = "BitbucketServerRegistry", storages = [
    Storage("bitbucket-server-registry.xml", deprecated = true),
    Storage(SETTINGS_STORAGE_FILE)
])
class DefaultBitbucketServerRegistry : BitbucketServerRegistry, PersistentStateComponent<Element> {
    private val lookupMap: MutableMap<BitbucketServerCoordinates, BitbucketServer> = mutableMapOf()

    override fun add(scheme: UriScheme, hostname: String, port: Int, path: String, server: BitbucketServer) {
        lookupMap.putIfAbsent(BitbucketServerCoordinates(scheme, hostname, port, path), server)
    }

    override fun lookup(scheme: UriScheme, hostname: String, port: Int, path: String) =
            lookupMap[BitbucketServerCoordinates(scheme, hostname, port, path)]

    override fun getState() =
            Element("BitbucketServerRegistry").apply {
                addContent(Element("instances").apply {
                    lookupMap.values
                            .sortedBy { it.uuid }
                            .forEach { server ->
                                addContent(Element("instance").apply {
                                    setAttribute("uuid", server.uuid.toString())
                                    setAttribute("baseUri", server.baseUrl.toString())
                                })
                            }
                })
                addContent(Element("lookup").apply {
                    lookupMap.toSortedMap()
                            .forEach { (coordinates, bitbucket) ->
                                addContent(Element("record").apply {
                                    setAttribute("scheme", coordinates.scheme.toString())
                                    setAttribute("hostname", coordinates.hostname)
                                    setAttribute("port", coordinates.port.toString())
                                    setAttribute("path", coordinates.path)
                                    setAttribute("uuid", bitbucket.uuid.toString())
                                })
                            }
                })
            }

    override fun loadState(element: Element) {
        // Legacy format contains random UUID for each server
        val instances = loadInstances(element) {
            val baseUri = it.getUrl("baseUri") ?: return@loadInstances null
            val server = BitbucketServer(baseUri)
            val uuid = it.getUuid("uuid") ?: server.uuid
            uuid to server
        }.toMap()

        lookupMap.clear()
        element.getChild("lookup").getChildren("record")
                .forEach {
                    val uriScheme = it.getUriScheme("scheme")
                    val hostname = it.getString("hostname")
                    val port = it.getInt("port")
                    val path = it.getString("path")
                    val uuid = it.getUuid("uuid")
                    val server = instances[uuid]
                    if (uriScheme != null && hostname != null && port != null && path != null && server != null) {
                        lookupMap.put(BitbucketServerCoordinates(uriScheme, hostname, port, path), server)
                    }
                }
    }

    data class BitbucketServerCoordinates(val scheme: UriScheme = HTTP, val hostname: String, val port: Int, val path: String) : Comparable<BitbucketServerCoordinates> {
        override fun compareTo(other: BitbucketServerCoordinates) =
                Comparator.comparing(Function<BitbucketServerCoordinates, String> { t -> t.hostname })
                        .thenComparing { t -> t.path }
                        .thenComparing { t -> t.port }
                        .thenComparing { t -> t.scheme }
                        .compare(this, other)
    }
}

private fun <T : Any> loadInstances(element: Element, createInstance: (Element) -> T?) =
        element.getChild("instances")
                ?.getChildren("instance")
                ?.mapNotNull { createInstance(it) }
                ?: listOf()
