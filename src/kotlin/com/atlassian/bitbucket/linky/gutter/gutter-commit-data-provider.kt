package com.atlassian.bitbucket.linky.gutter

import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.LinkyFileActionContext
import com.atlassian.bitbucket.linky.actions.linkyActionContext
import com.atlassian.bitbucket.linky.revisionHash
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.vcs.annotate.FileAnnotation
import com.intellij.openapi.vcs.annotate.UpToDateLineNumberListener

interface GutterCommitDataProvider : UpToDateLineNumberListener {
    fun getGutterCommitData(event: AnActionEvent): GutterCommitData?
}

data class GutterCommitData(val ctx: LinkyFileActionContext,
                            val actualLineNumber: Int? = null,
                            val revision: Revision? = null)

class DefaultGutterCommitDataProvider(private val annotation: FileAnnotation) : GutterCommitDataProvider {
    private var lineNumber: Int = -1

    override fun consume(lineNumber: Int) {
        this.lineNumber = lineNumber
    }

    override fun getGutterCommitData(event: AnActionEvent): GutterCommitData? {
        val ctx = event.linkyActionContext()
        return if (ctx is LinkyFileActionContext) {
            getLineNumberAndRevision()
                    ?.let { (line, revision) -> GutterCommitData(ctx, line, revision) }
                    ?: GutterCommitData(ctx)
        } else {
            null
        }
    }

    private fun getLineNumberAndRevision(): Pair<Int, Revision>? =
            lineNumber.takeIf { it >= 0 }?.let {
                annotation.getLineRevisionNumber(it)?.revisionHash?.let { revision -> Pair(it, revision) }
            }
}
