package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.blame.LineBlamer
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.vcs.history.VcsRevisionNumber
import com.intellij.openapi.vfs.VirtualFile
import git4idea.GitRevisionNumber
import org.zmlx.hg4idea.HgRevisionNumber

typealias RelativePath = String

class LinkyFile(val virtualFile: VirtualFile,
                val repository: Repository,
                val revision: Revision,
                val relativePath: RelativePath,
                private val lineBlamer: LineBlamer) {
    val name = virtualFile.name

    fun blameLine(lineNumber: Int) = lineBlamer.blameLine(this, lineNumber)
}

val VcsRevisionNumber.revisionHash: Revision?
    get() = when {
        isGitAvailable() && this is GitRevisionNumber -> rev
        isHgAvailable() && this is HgRevisionNumber -> changeset
        else -> null
    }
