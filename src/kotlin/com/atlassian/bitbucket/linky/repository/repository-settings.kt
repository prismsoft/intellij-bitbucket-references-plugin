package com.atlassian.bitbucket.linky.repository

import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import org.jdom.Element
import java.util.concurrent.ConcurrentHashMap

enum class SettingKey(val key: String) {
    PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME("pull_request_default_target_branch_name"),
    ALWAYS_LINK_TO_SELECTED_REMOTE("always_link_to_remote")
}

interface RepositorySettings {
    fun getProperty(repository: Repository, key: SettingKey, defaultValue: String? = null): String?
    fun setProperty(repository: Repository, key: SettingKey, value: String)
    fun removeProperty(repository: Repository, key: SettingKey)
}

@State(name = "BitbucketRepositoryProperties", storages = arrayOf(Storage("bitbucket-repo-properties.xml")))
class DefaultRepositorySettings : RepositorySettings, PersistentStateComponent<Element> {
    private val properties: MutableMap<String, MutableMap<String, String>> = ConcurrentHashMap()

    override fun getProperty(repository: Repository, key: SettingKey, defaultValue: String?) =
            properties[repository.getRelativePathInProject()]?.get(key.key) ?: defaultValue

    override fun setProperty(repository: Repository, key: SettingKey, value: String) {
        val repoKey = repository.getRelativePathInProject()
        var repoSettings = properties[repoKey]
        if (repoSettings == null) {
            repoSettings = mutableMapOf()
            properties.put(repoKey, repoSettings)
        }
        repoSettings.put(key.key, value)
    }

    override fun removeProperty(repository: Repository, key: SettingKey) {
        properties[repository.getRelativePathInProject()]?.remove(key.key)
    }

    override fun getState(): Element {
        return Element("BitbucketRepositoryProperties").apply {
            properties.forEach { entry ->
                if (entry.value.isNotEmpty()) {
                    addContent(Element("repository").apply {
                        setAttribute("relativeRootPath", entry.key)
                        addContent(Element("properties").apply {
                            entry.value.forEach { property ->
                                addContent(Element("property").apply {
                                    setAttribute("name", property.key)
                                    addContent(property.value)
                                })
                            }
                        })
                    })
                }
            }
        }
    }

    override fun loadState(element: Element) {
        properties.clear()
        element.getChildren("repository")
                .forEach {
                    val repoProperties = ConcurrentHashMap<String, String>()
                    it.getChild("properties")
                            .getChildren("property")
                            .forEach { repoProperties.put(it.getAttributeValue("name"), it.value) }
                    properties.put(it.getAttributeValue("relativeRootPath"), repoProperties)
                }
    }
}
