package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.isGitAvailable
import com.atlassian.bitbucket.linky.isHgAvailable
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.AbstractVcs
import com.intellij.openapi.vfs.VfsUtil
import git4idea.GitUtil
import git4idea.repo.GitRepository
import org.apache.commons.lang.StringUtils
import org.zmlx.hg4idea.repo.HgRepository
import org.zmlx.hg4idea.util.HgUtil

// TODO get rid of this, just return all repositories
interface RepositoriesTraverser {
    fun traverseRepositories(): Collection<Repository>
    fun <T> traverseRepositories(onAnyRepository: (Repository) -> T): Collection<T>
    fun <T> traverseRepositories(onGitRepository: (Repository) -> T, onHgRepository: (Repository) -> T): Collection<T>
}

class ProjectRepositoriesTraverser(private val project: Project) : RepositoriesTraverser {

    override fun traverseRepositories(): Collection<Repository> = traverseRepositories { it }

    override fun <T> traverseRepositories(onAnyRepository: (Repository) -> T) = traverseRepositories(onAnyRepository, onAnyRepository)

    override fun <T> traverseRepositories(onGitRepository: (Repository) -> T, onHgRepository: (Repository) -> T): Collection<T> {
        val gitItems = if (isGitAvailable()) {
            GitUtil.getRepositoryManager(project)
                    .repositories
                    .map { onGitRepository.invoke(it) }
        } else {
            emptyList()
        }
        val hgItems = if (isHgAvailable()) {
            HgUtil.getRepositoryManager(project)
                    .repositories
                    .map { onHgRepository.invoke(it) }
        } else {
            emptyList()
        }
        return gitItems + hgItems
    }
}

fun AbstractVcs<*>.isGit() = this.keyInstanceMethod.name == "Git"
fun AbstractVcs<*>.isHg() = this.keyInstanceMethod.name == "hg4idea"

fun Repository.isGit() = this.vcs.isGit()
fun Repository.isHg() = this.vcs.isHg()

fun Repository.getCurrentRemoteUrl(): RemoteUrl? =
        if (isGit() && this is GitRepository) {
            getCurrentRemoteUrl()
        } else if (isHg() && this is HgRepository) {
            getCurrentRemoteUrl()
        } else null

fun Repository.getRemoteUrls(): List<RemoteUrl> =
        if (isGit() && this is GitRepository) {
            getRemoteUrls()
        } else if (isHg() && this is HgRepository) {
            getRemoteUrls()
        } else {
            emptyList()
        }

fun Repository.getOrderedRemoteUrls(): List<RemoteUrl> =
        getCurrentRemoteUrl()
                ?.let { current -> listOf(current) + getRemoteUrls().filterNot { it == current } }
                ?: getRemoteUrls()

fun Repository.getRelativePathInProject(): String =
        StringUtils.defaultIfEmpty(VfsUtil.getRelativePath(root, project.baseDir), "/")

fun Repository.hasRevisionBeenPushed(revision: Revision): Boolean =
// if push status is unknown, assume the happy case
        when {
            this.isGit() && this is GitRepository -> hasRevisionBeenPushed(revision)
            this.isHg() && this is HgRepository -> hasRevisionBeenPushed(revision)
            else -> true
        }
