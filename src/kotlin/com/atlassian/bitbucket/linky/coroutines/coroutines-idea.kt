package com.atlassian.bitbucket.linky.coroutines

import com.atlassian.bitbucket.linky.logger
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import kotlinx.coroutines.*
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext

private val log = logger()

/**
 * Copied over [org.jetbrains.kotlin.idea.core.util.EDT] from Kotlin `idea/idea-core` module
 *
 * See https://github.com/JetBrains/kotlin/blob/master/idea/idea-core/src/org/jetbrains/kotlin/idea/core/util/CoroutineUtils.kt
 */
@UseExperimental(ExperimentalCoroutinesApi::class)
private object EDT : CoroutineDispatcher() {
    override fun isDispatchNeeded(context: CoroutineContext): Boolean {
        return !ApplicationManager.getApplication().isDispatchThread
    }

    override fun dispatch(context: CoroutineContext, block: Runnable) {
        val modalityState = context[ModalityStateElement]?.modalityState ?: ModalityState.defaultModalityState()
        ApplicationManager.getApplication().invokeLater(block, modalityState)
    }

    class ModalityStateElement(val modalityState: ModalityState) : AbstractCoroutineContextElement(Key) {
        companion object Key : CoroutineContext.Key<ModalityStateElement>
    }

    operator fun invoke(project: Project) = this + project.cancelOnDisposal
}

// job that is cancelled when the project is disposed
private val Project.cancelOnDisposal: Job
    get() = service<ProjectJob>().sharedJob

private class ProjectJob(project: Project) {
    internal val sharedJob: Job = Job()

    init {
        Disposer.register(project, Disposable {
            sharedJob.cancel()
        })
    }
}

// TODO this guy doesn't catch all errors right now
private val loggingExceptionHandler = CoroutineExceptionHandler { _, throwable ->
    log.error("Uncaught coroutine exception", throwable)
}

val UI = EDT + loggingExceptionHandler
val Background = Dispatchers.Default + loggingExceptionHandler
val IO = Dispatchers.IO + loggingExceptionHandler
