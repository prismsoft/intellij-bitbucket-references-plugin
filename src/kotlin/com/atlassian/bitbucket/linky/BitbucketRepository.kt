package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.util.resolve
import com.intellij.dvcs.repo.Repository
import io.ktor.http.Url

sealed class BitbucketRepository(val repository: Repository,
                                 val remoteUrl: RemoteUrl,
                                 val slug: String) {
    abstract val baseUri: Url

    abstract val fullSlug: String

    class Cloud(repository: Repository,
                remoteUrl: RemoteUrl,
                val hosting: BitbucketCloud,
                val workspaceId: String,
                slug: String) : BitbucketRepository(repository, remoteUrl, slug) {
        override val baseUri: Url
            get() = hosting.baseUrl.resolve(fullSlug)
        override val fullSlug: String
            get() = "$workspaceId/$slug"

        override fun toString(): String {
            return "BitbucketCloudRepository{$fullSlug}"
        }
    }

    class Server(repository: Repository,
                 remoteUrl: RemoteUrl,
                 val hosting: BitbucketServer,
                 val projectKey: String,
                 slug: String) : BitbucketRepository(repository, remoteUrl, slug) {
        override val baseUri: Url
            get() = hosting.baseUrl.resolve("projects/$projectKey/repos/$slug")
        override val fullSlug: String
            get() = "$projectKey/$slug"

        override fun toString(): String {
            return "BitbucketServerRepository{$fullSlug}"
        }
    }
}
