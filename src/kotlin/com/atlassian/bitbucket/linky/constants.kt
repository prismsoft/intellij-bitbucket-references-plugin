package com.atlassian.bitbucket.linky

import java.net.URI

const val LINKY_ISSUES_URL = "https://bitbucket.org/atlassianlabs/intellij-bitbucket-references-plugin/issues?status=new&status=open"

/**
 * Key that should be used to create {@link org.slf4j.Logger} instance.
 * It is easier to filter {@code idea.log} by just one expression
 * while troubleshooting.
 */
const val LOGGER_NAME = "BitbucketReferences"

internal const val SETTINGS_STORAGE_FILE = "bitbucket-linky.xml"
