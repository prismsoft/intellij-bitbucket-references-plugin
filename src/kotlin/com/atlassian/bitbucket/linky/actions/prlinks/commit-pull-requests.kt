package com.atlassian.bitbucket.linky.actions.prlinks

import com.atlassian.bitbucket.cloud.rest.commit.PullRequestLinksIndexingException
import com.atlassian.bitbucket.cloud.rest.commit.PullRequestLinksNotEnabledException
import com.atlassian.bitbucket.cloud.rest.oauth.OAuthNotConfiguredException
import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.LinkyCommitActionContext
import com.atlassian.bitbucket.linky.actions.LinkyFileActionContext
import com.atlassian.bitbucket.linky.actions.linkyActionContext
import com.atlassian.bitbucket.linky.actions.logActionUsage
import com.atlassian.bitbucket.linky.coroutines.UI
import com.atlassian.bitbucket.linky.gutter.DefaultGutterCommitDataProvider
import com.atlassian.bitbucket.linky.gutter.GutterCommitDataProvider
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.PullRequestFind
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.repository.hasRevisionBeenPushed
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.annotate.AnnotationGutterActionProvider
import com.intellij.openapi.vcs.annotate.FileAnnotation
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowId
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.ui.content.Content
import com.intellij.ui.content.ContentFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import java.io.IOException
import kotlin.coroutines.CoroutineContext

private val log = logger()

open class FindCommitRelatedPullRequestsAction : CoroutineScope, DumbAwareAction(
        "Find related Pull Requests", "Find Pull Requests that reviewed the selected commit", PullRequestFind) {

    override val coroutineContext: CoroutineContext get() = UI

    override fun update(event: AnActionEvent) {
        val repositoryRevision = getBitbucketRepositoryRevision(event)
        if (repositoryRevision == null) {
            event.presentation.isEnabledAndVisible = false
        } else {
            val (bbRepo, revision) = repositoryRevision
            event.presentation.isEnabledAndVisible = bbRepo.repository.hasRevisionBeenPushed(revision)
        }
    }

    override fun actionPerformed(event: AnActionEvent) {
        launch {
            val (bitbucketRepository, revision) =
                    getBitbucketRepositoryRevision(event) ?: return@launch
            showPullRequestsForCommit(bitbucketRepository, revision)
            logActionUsage("FindRelatedPullRequests", event.place, bitbucketRepository)
        }
    }

    internal open fun getBitbucketRepositoryRevision(event: AnActionEvent): Pair<BitbucketRepository, Revision>? {
        val ctx = event.linkyActionContext()
        val bitbucketRepository = ctx.getBitbucketRepository() ?: return null
        return when (ctx) {
            is LinkyCommitActionContext -> Pair(bitbucketRepository, ctx.revision)
            is LinkyFileActionContext -> Pair(bitbucketRepository, ctx.linkyFile.revision)
            else -> null
        }
    }

    private suspend fun getRelatedPullRequests(repository: BitbucketRepository, revision: Revision) =
            try {
                when (repository) {
                    is BitbucketRepository.Cloud -> fetchRelatedPullRequests(repository, revision)
                    is BitbucketRepository.Server -> fetchRelatedPullRequests(repository, revision)
                }
            } catch (e: Exception) {
                throw FetchRelatedPullRequestsException(e)
            }

    @UseExperimental(ExperimentalCoroutinesApi::class)
    private suspend fun showPullRequestsForCommit(repository: BitbucketRepository, revision: Revision) {
        val project = repository.repository.project
        // TODO show progress bar
        val pullRequests = try {
            getRelatedPullRequests(repository, revision)
        } catch (e: FetchRelatedPullRequestsException) {
            val prPanel = showRelatedPullRequestsToolWindow(project, revision)
            showError(repository, prPanel, revision, e.cause)
            null
        }

        if (pullRequests != null) {
            val prPanel = showRelatedPullRequestsToolWindow(project, revision)
            prPanel.showPullRequests(pullRequests.sortedBy { it.id })
        }
    }

    private fun showRelatedPullRequestsToolWindow(project: Project, revision: Revision): CommitPullRequestsPanel {
        val vcsToolWindow = ToolWindowManager.getInstance(project).getToolWindow(ToolWindowId.VCS)
        val (commitPullRequestsPanel, content) = vcsToolWindow.getOrCreatePullRequestsPanel(revision)

        commitPullRequestsPanel.showLoading()
        vcsToolWindow.contentManager.setSelectedContent(content, true)
        if (vcsToolWindow.isActive.not()) {
            vcsToolWindow.activate(null)
        }

        return commitPullRequestsPanel
    }

    private fun ToolWindow.getOrCreatePullRequestsPanel(revision: Revision): Pair<CommitPullRequestsPanel, Content> {
        val revisionDisplayName = revision.substring(0, minOf(revision.length, 7))
        val tabTitle = "Pull Requests: $revisionDisplayName"

        val existingContent = contentManager.contents.firstOrNull { it.displayName == tabTitle }
        return if (existingContent != null) {
            existingContent.component as CommitPullRequestsPanel to existingContent
        } else {
            val commitPullRequestsPanel = CommitPullRequestsPanel(contentManager)
            val content = ContentFactory.SERVICE.getInstance()
                    .createContent(commitPullRequestsPanel, tabTitle, true)
            contentManager.addContent(content)
            commitPullRequestsPanel to content
        }
    }

    private fun <E : Throwable> showError(repository: BitbucketRepository,
                                          prPanel: CommitPullRequestsPanel,
                                          revision: Revision,
                                          error: E) {
        // TODO timeout
        when (error) {
            is PullRequestLinksIndexingException -> prPanel.showPullRequestsAreBeingIndexed()
            is PullRequestLinksNotEnabledException -> prPanel.showPullRequestsNeedToBeIndexed(repository as BitbucketRepository.Cloud, revision)
            is OAuthNotConfiguredException -> {
                prPanel.showAuthenticationRequiredError()
                throw error
            }
            is IOException -> {
                log.warn("Failed to fetch related Pull Requests for $repository", error)
                prPanel.showNetworkError()
            }
            else -> {
                log.error("Failed to fetch related Pull Requests for $repository", error)
                prPanel.showGenericError()
            }
        }
    }
}

class FindCommitRelatedPullRequestsGutterActionProvider : AnnotationGutterActionProvider {
    override fun createAction(annotation: FileAnnotation): AnAction =
            object : FindCommitRelatedPullRequestsAction(),
                    GutterCommitDataProvider by DefaultGutterCommitDataProvider(annotation) {

                override fun getBitbucketRepositoryRevision(event: AnActionEvent): Pair<BitbucketRepository, Revision>? {
                    val (bitbucketRepository, _) = super.getBitbucketRepositoryRevision(event) ?: return null
                    val (_, _, lineRevision) = getGutterCommitData(event) ?: return null
                    return lineRevision?.let { Pair(bitbucketRepository, lineRevision) }
                }
            }
}

private class FetchRelatedPullRequestsException(override val cause: Throwable) : RuntimeException(cause)
