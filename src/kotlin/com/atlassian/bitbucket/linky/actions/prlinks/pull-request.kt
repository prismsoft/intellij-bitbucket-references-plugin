package com.atlassian.bitbucket.linky.actions.prlinks

import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import java.net.URI
import java.time.Instant

private val commonmarkHtmlRenderer = HtmlRenderer.builder().build()
private val commonmarkParser = Parser.builder().build()

data class RelatedPullRequest(
        val id: Long,
        val title: String,
        val description: String?,
        val sourceBranchName: String,
        val destinationBranchName: String,
        val state: String,
        val authorName: String?,
        val createdDate: Instant,
        val uri: URI
) {
    fun html(): String = """
        <h4>$title</h4>
        <div class="branches">
            <code>$sourceBranchName</code> &rArr; <code>$destinationBranchName</code>
        </div>
        <hr/>
        ${description?.let { commonmarkHtmlRenderer.render(commonmarkParser.parse(it)) } ?: ""}
        """.trimIndent()
}

fun com.atlassian.bitbucket.cloud.rest.pullrequest.PullRequest.toRelatedPullRequest() =
        RelatedPullRequest(
                id,
                title,
                description,
                source.branchName,
                destination.branchName,
                state.name,
                author?.let { if (it.name.isBlank()) it.nickname else it.name },
                createdDate,
                links.getValue("html").href
        )

fun com.atlassian.bitbucket.server.rest.pullrequest.PullRequest.toRelatedPullRequest() =
        RelatedPullRequest(
                id,
                title,
                description,
                source.name,
                destination.name,
                state.name,
                author.name,
                createdDate,
                links.getValue("self").href
        )
