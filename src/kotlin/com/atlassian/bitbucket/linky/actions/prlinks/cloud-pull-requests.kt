package com.atlassian.bitbucket.linky.actions.prlinks

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.authenticated.CloudAuthenticator
import com.atlassian.bitbucket.linky.utils.fetchAllPagedItems
import com.intellij.openapi.components.service

internal suspend fun fetchRelatedPullRequests(repository: BitbucketRepository.Cloud, revision: Revision): List<RelatedPullRequest>? {
    val project = repository.repository.project
    val cloud = repository.hosting

    val cloudAuthHelper = project.service<CloudAuthenticator>()
    return cloudAuthHelper.call(cloud, "list related pull requests") { client ->
        fetchAllPagedItems(
                client.repository(repository.workspaceId, repository.slug).commit(revision).pagePullRequests(),
                { page -> client.nextPage(page) },
                { page -> page.items }
        ).map {
            it.toRelatedPullRequest()
        }
    }
}
