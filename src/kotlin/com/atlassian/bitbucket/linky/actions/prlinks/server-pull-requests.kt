package com.atlassian.bitbucket.linky.actions.prlinks

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.actions.authenticated.ServerAuthenticator
import com.atlassian.bitbucket.linky.utils.fetchAllPagedItems
import com.intellij.openapi.components.service

suspend fun fetchRelatedPullRequests(repository: BitbucketRepository.Server, revision: Revision): List<RelatedPullRequest>? {
    val project = repository.repository.project
    val server = repository.hosting

    val serverAuthHelper = project.service<ServerAuthenticator>()
    return serverAuthHelper.call(server, "list related pull requests") { client ->
        fetchAllPagedItems(
                client.repository(repository.projectKey, repository.slug).commit(revision).pagePullRequests(),
                { page -> client.nextPage(page) },
                { page -> page.items }
        ).map {
            it.toRelatedPullRequest()
        }
    }
}
