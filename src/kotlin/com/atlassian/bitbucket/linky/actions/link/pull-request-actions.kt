package com.atlassian.bitbucket.linky.actions.link

import com.atlassian.bitbucket.linky.BitbucketLinky
import com.atlassian.bitbucket.linky.actions.*
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.PullRequestCreate
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project

class BitbucketCreatePullRequestAction : DumbAwareAction(
        "Bitbucket Pull Request", "Create Bitbucket Pull Request", PullRequestCreate) {

    override fun actionPerformed(event: AnActionEvent) {
        val ctx = event.linkyActionContext()
        when (ctx) {
            is LinkyFileActionContext -> navigateToBitbucket(ctx.getLinky(), ctx.project, event.place)
            is LinkyRepositoryActionContext -> navigateToBitbucket(ctx.getLinky(), ctx.project, event.place)
            else -> return
        }
    }

    override fun update(event: AnActionEvent) {
        val actionContext = event.linkyActionContext()
        if (actionContext is LinkyFileActionContext || actionContext is LinkyRepositoryActionContext) {
            val linky = actionContext.getLinky()
            if (linky != null) {
                event.presentation.isVisible = true
                if (linky.getPullRequestUri().isPresent) {
                    event.presentation.isEnabled = true
                } else {
                    event.presentation.isEnabled = false
                    event.presentation.description = "You should be on a branch to create a Pull Request"
                }
            } else {
                event.presentation.isEnabledAndVisible = false
            }
        } else {
            event.presentation.isEnabledAndVisible = false
        }
    }

    private fun navigateToBitbucket(linky: BitbucketLinky?, project: Project, eventPlace: String) =
            linky?.getPullRequestUri()?.ifPresent {
                openUriInBrowser(project, Link(it, "Create Pull Request form"))
                logActionUsage("CreatePullRequest", eventPlace, linky.getRepository())
            }
}
