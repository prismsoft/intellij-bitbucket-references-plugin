package com.atlassian.bitbucket.linky.actions.authenticated

import com.atlassian.bitbucket.linky.rest.PersonalAccessToken
import com.atlassian.bitbucket.linky.utils.createHttpClient
import com.atlassian.bitbucket.server.rest.*
import com.atlassian.bitbucket.server.rest.personalaccesstoken.AccessLevel.READ
import com.atlassian.bitbucket.server.rest.personalaccesstoken.Permission.ProjectPermission
import com.atlassian.bitbucket.server.rest.personalaccesstoken.Permission.RepositoryPermission
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ModalityState
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.EmptyProgressIndicator
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.util.Disposer
import com.intellij.ui.AnimatedIcon
import com.intellij.ui.DocumentAdapter
import com.intellij.ui.JBColor
import com.intellij.ui.components.JBTextField
import com.intellij.ui.components.fields.ExtendableTextComponent
import com.intellij.ui.components.fields.ExtendableTextField
import com.intellij.ui.components.labels.LinkLabel
import com.intellij.ui.components.panels.Wrapper
import com.intellij.util.ui.JBUI
import com.intellij.util.ui.UI.PanelFactory.grid
import com.intellij.util.ui.UI.PanelFactory.panel
import com.intellij.util.ui.UIUtil
import kotlinx.coroutines.runBlocking
import java.awt.Component
import javax.swing.*
import javax.swing.event.DocumentEvent

private const val DEFAULT_TOKEN_NAME = "Bitbucket Linky IntelliJ plugin"

class BitbucketServerLoginDialog(private val project: Project,
                                 private val server: BitbucketServer,
                                 parent: Component? = null)
    : DialogWrapper(project, parent, false, IdeModalityType.PROJECT) {

    private val serverTextField = ExtendableTextField().apply {
        isEditable = false
        isEnabled = false
        isFocusable = false
        text = server.baseUrl.toString()
    }
    private val centerPanel = Wrapper()
    private val southAdditionalPanel = Wrapper().apply { JBUI.Borders.emptyRight(UIUtil.DEFAULT_HGAP) }

    private var passwordUi = LoginPasswordCredentialsUI()
    private var tokenUi = TokenCredentialsUI()

    private lateinit var currentUi: CredentialsUI

    private var progressIndicator: ProgressIndicator? = null
    private val progressIcon = AnimatedIcon.Default()
    private val progressExtension = ExtendableTextComponent.Extension { progressIcon }

    private var tokenAcquisitionError: ValidationInfo? = null

    internal lateinit var token: String

    init {
        title = "Log in to Bitbucket Server"
        applyUi(passwordUi)
        init()
        Disposer.register(disposable, Disposable { progressIndicator?.cancel() })
    }

    override fun doOKAction() {
        setBusy(true)
        tokenAcquisitionError = null

        service<ProgressManager>().runProcessWithProgressAsynchronously(object : Task.Backgroundable(project, "Not Visible") {
            override fun run(indicator: ProgressIndicator) {
                // This runs asynchronously in the background, and blocking here is not a big deal
                token = runBlocking { currentUi.acquireToken(server, indicator).value }
            }

            override fun onSuccess() {
                close(OK_EXIT_CODE, true)
                setBusy(false)
            }

            override fun onThrowable(error: Throwable) {
                startTrackingValidation()
                tokenAcquisitionError = currentUi.handleAcquireError(error)
                setBusy(false)
            }
        }, progressIndicator!!)
    }

    private fun setBusy(busy: Boolean) {
        if (busy) {
            if (!serverTextField.extensions.contains(progressExtension)) serverTextField.addExtension(progressExtension)
            progressIndicator?.cancel()
            progressIndicator = EmptyProgressIndicator(ModalityState.stateForComponent(window))
            Disposer.register(disposable, Disposable { progressIndicator?.cancel() })
        } else {
            serverTextField.removeExtension(progressExtension)
            progressIndicator = null
        }
        serverTextField.isEnabled = !busy
        currentUi.setBusy(busy)
    }

    override fun createSouthAdditionalPanel(): Wrapper = southAdditionalPanel
    override fun createCenterPanel() = centerPanel
    override fun getPreferredFocusedComponent(): JComponent = currentUi.getPreferredFocus()

    private fun applyUi(ui: CredentialsUI) {
        currentUi = ui
        centerPanel.setContent(currentUi.getPanel())
        southAdditionalPanel.setContent(currentUi.getSouthPanel())
        setOKButtonText(currentUi.getActionText())
        currentUi.getPreferredFocus().requestFocus()
        tokenAcquisitionError = null
    }

    override fun doValidateAll(): List<ValidationInfo> =
            listOf(currentUi.getValidator(), { tokenAcquisitionError }).mapNotNull { it() }

    private fun JTextField.resetTokenAcquisitionErrorOnChange() =
            document.addDocumentListener(object : DocumentAdapter() {
                override fun textChanged(e: DocumentEvent) {
                    tokenAcquisitionError = null
                }
            })


    private inner class LoginPasswordCredentialsUI : CredentialsUI {
        private val usernameField = JBTextField().apply { resetTokenAcquisitionErrorOnChange() }
        private val passwordField = JPasswordField().apply { resetTokenAcquisitionErrorOnChange() }
        private val tokenNameField = JBTextField()
        private val contextHelp = JLabel()

        private val switchUiLink = LinkLabel.create("Have access token handy?") { applyUi(tokenUi) }

        init {
            contextHelp.apply {
                text = "Password is not saved and used only to acquire Bitbucket Server Personal access token."
                foreground = JBColor.GRAY
            }

            tokenNameField.text = DEFAULT_TOKEN_NAME
        }

        override fun getPanel() = grid()
                .add(panel(serverTextField).withLabel("Server:"))
                .add(panel(usernameField).withLabel("Username:"))
                .add(panel(passwordField).withLabel("Password:"))
                .add(panel(tokenNameField).withLabel("Token name:"))
                .add(panel(contextHelp)).createPanel()

        override fun getPreferredFocus() = usernameField

        override fun getValidator() = chain(
                { notBlank(usernameField, "Username cannot be empty") },
                { notBlank(passwordField, "Password cannot be empty") }
        )

        override fun getSouthPanel() = switchUiLink

        override fun getActionText() = "Acquire token"

        override suspend fun acquireToken(server: BitbucketServer, indicator: ProgressIndicator): PersonalAccessToken {
            val username = usernameField.text
            val client = BitbucketServerApi {
                instance = server
                authentication = Authentication.Basic(username, String(passwordField.password))
                httpClient { createHttpClient(server.apiBaseUrl) }
            }

            val userTokenName = tokenNameField.text
            val tokenName = if (userTokenName.isNullOrBlank()) tokenNameField.emptyText.text else userTokenName

            val token = client.personalAccessTokens(username)
                    .create(tokenName, listOf(ProjectPermission(READ), RepositoryPermission(READ)))

            return PersonalAccessToken(token.token)
        }

        override fun handleAcquireError(error: Throwable): ValidationInfo? = when (error) {
            is ForbiddenException -> ValidationInfo("Insufficient access level", usernameField)
            is UnauthorizedException -> ValidationInfo("Incorrect credentials", passwordField).withOKEnabled()
            else -> ValidationInfo(error.message ?: "Failed to validate personal access token").withOKEnabled()
        }

        override fun setBusy(busy: Boolean) {
            usernameField.isEnabled = !busy
            passwordField.isEnabled = !busy
            tokenNameField.isEnabled = !busy
            contextHelp.isEnabled = !busy
            switchUiLink.isEnabled = !busy
        }
    }

    private inner class TokenCredentialsUI : CredentialsUI {
        private val tokenField = JBTextField().apply { resetTokenAcquisitionErrorOnChange() }

        private val switchUiLink = LinkLabel.create("Log in with username and password") { applyUi(passwordUi) }

        override fun getPanel() = grid()
                .add(panel(serverTextField).withLabel("Server:"))
                .add(panel(tokenField).withLabel("Token:")).createPanel()

        override fun getPreferredFocus() = tokenField

        override fun getValidator() = { notBlank(tokenField, "Token cannot be empty") }

        override fun getSouthPanel() = switchUiLink

        override fun getActionText() = "Check token"

        override suspend fun acquireToken(server: BitbucketServer, indicator: ProgressIndicator): PersonalAccessToken {
            val token = tokenField.text
            val client = BitbucketServerApi {
                instance = server
                authentication = Authentication.Bearer(token)
                httpClient { createHttpClient(server.apiBaseUrl) }
            }

            client.testConnectivity().authenticatedResource()

            return PersonalAccessToken(token)
        }

        override fun handleAcquireError(error: Throwable): ValidationInfo? =
                when (error) {
                    is ForbiddenException -> ValidationInfo("Insufficient access level", tokenField)
                    is UnauthorizedException -> ValidationInfo("Incorrect personal access token", tokenField).withOKEnabled()
                    else -> ValidationInfo(error.message ?: "Failed to validate personal access token").withOKEnabled()
                }

        override fun setBusy(busy: Boolean) {
            tokenField.isEnabled = !busy
            switchUiLink.isEnabled = !busy
        }
    }
}

typealias Validator = () -> ValidationInfo?

private fun chain(vararg validators: Validator): Validator = { validators.asSequence().mapNotNull { it() }.firstOrNull() }
private fun notBlank(textField: JTextField, message: String): ValidationInfo? =
        if (textField.text.isNullOrBlank()) ValidationInfo(message, textField) else null

private interface CredentialsUI {
    fun getPanel(): JPanel
    fun getSouthPanel(): JComponent
    fun getPreferredFocus(): JComponent
    fun getValidator(): Validator
    fun getActionText(): String

    suspend fun acquireToken(server: BitbucketServer, indicator: ProgressIndicator): PersonalAccessToken
    fun handleAcquireError(error: Throwable): ValidationInfo?

    fun setBusy(busy: Boolean)
}
