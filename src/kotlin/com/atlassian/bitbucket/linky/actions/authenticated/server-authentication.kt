package com.atlassian.bitbucket.linky.actions.authenticated

import com.atlassian.bitbucket.linky.hosting.uuid
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.BitbucketAuthenticationException
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.linky.rest.credentialsAttributes
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.BitbucketServerApi
import com.atlassian.bitbucket.server.rest.UnauthorizedException
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageDialogBuilder
import com.intellij.openapi.ui.Messages
import java.util.concurrent.atomic.AtomicReference

private val log = logger()

interface ServerAuthenticator {
    suspend fun run(server: BitbucketServer,
                    actionDescription: String,
                    action: suspend (BitbucketServerApi) -> Unit) {
        call(server, actionDescription, action)
    }

    suspend fun <R> call(server: BitbucketServer,
                         actionDescription: String,
                         action: suspend (BitbucketServerApi) -> R): R?
}

class DefaultServerAuthenticator(private val project: Project,
                                 private val passwordSafe: PasswordSafe,
                                 private val restClientProvider: BitbucketRestClientProvider) : ServerAuthenticator {

    override suspend fun <R> call(server: BitbucketServer, actionDescription: String, action: suspend (BitbucketServerApi) -> R): R? {
        suspend fun invokeAction(): R =
                restClientProvider.bitbucketServerRestClient(server, true)
                        .use { restClient ->
                            action(restClient)
                        }

        suspend fun handleAuthConfigurationError(e: Exception): R? {
            log.debug(e.message)
            // This time we allow potential authentication error to bubble up
            return if (configureAuth(server, actionDescription)) {
                invokeAction()
            } else null
        }

        return try {
            invokeAction()
        } catch (e: BitbucketAuthenticationException) {
            handleAuthConfigurationError(e)
        } catch (e: UnauthorizedException) {
            handleAuthConfigurationError(e)
        }
    }

    private fun configureAuth(server: BitbucketServer, actionDescription: String): Boolean {
        if (userWantsToConfigureAuth(actionDescription)) {
            val dialog = BitbucketServerLoginDialog(project, server)
            if (dialog.showAndGet()) {
                passwordSafe.set(credentialsAttributes(server),
                        Credentials(server.uuid.toString(), dialog.token))
                return true
            }
        }
        log.info("User didn't configure Personal Access Token for $server")
        return false
    }

    private fun userWantsToConfigureAuth(actionDescription: String): Boolean {
        val userWantsToConfigureOAuth = AtomicReference<Boolean>()
        ApplicationManager.getApplication().invokeAndWait {
            val result = MessageDialogBuilder
                    .yesNo("Authentication required",
                            """|Linky needs to access Bitbucket on your behalf to $actionDescription.
                               |
                               |Do you want to create a personal access token now?
                            """.trimMargin())
                    .yesText("Configure access token")
                    .noText("Cancel")
                    .project(project)
                    .show()
            userWantsToConfigureOAuth.set(result == Messages.YES)
        }
        return userWantsToConfigureOAuth.get()
    }
}
