package com.atlassian.bitbucket.linky.actions.authenticated

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.BitbucketCloudApi
import com.atlassian.bitbucket.cloud.rest.oauth.OAuthAccessTokenLateRefreshException
import com.atlassian.bitbucket.cloud.rest.oauth.OAuthNotConfiguredException
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.BitbucketAuthenticationException
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.linky.rest.oauth.BitbucketCloudOAuthConfigurer
import com.atlassian.bitbucket.linky.rest.oauth.OAuthCallbackException
import com.google.common.base.Throwables
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageDialogBuilder
import com.intellij.openapi.ui.Messages
import java.util.concurrent.atomic.AtomicReference

private val log = logger()

interface CloudAuthenticator {
    suspend fun run(cloud: BitbucketCloud,
                    actionDescription: String,
                    action: suspend (BitbucketCloudApi) -> Unit) {
        call(cloud, actionDescription, action)
    }

    suspend fun <R> call(cloud: BitbucketCloud,
                         actionDescription: String,
                         action: suspend (BitbucketCloudApi) -> R): R?
}

class DefaultCloudAuthenticator(private val project: Project,
                                private val restClientProvider: BitbucketRestClientProvider,
                                private val cloudOAuthConfigurer: BitbucketCloudOAuthConfigurer) : CloudAuthenticator {

    override suspend fun <R> call(cloud: BitbucketCloud,
                                  actionDescription: String,
                                  action: suspend (BitbucketCloudApi) -> R): R? {
        suspend fun invokeAction(): R =
                restClientProvider.bitbucketCloudRestClient(cloud, true)
                        .use { restClient ->
                            action(restClient)
                        }

        suspend fun handleOAuthConfigurationError(e: Exception): R? {
            log.debug(e.message)
            // This time we allow potential authentication error to bubble up
            return if (configureOAuth(cloud, actionDescription)) {
                invokeAction()
            } else null
        }

        return try {
            invokeAction()
        } catch (e: BitbucketAuthenticationException) {
            handleOAuthConfigurationError(e)
        } catch (e: OAuthNotConfiguredException) {
            handleOAuthConfigurationError(e)
        } catch (e: OAuthAccessTokenLateRefreshException) {
            // Access token refreshed, but request can't be retried automatically due to non-empty body, so retry here
            invokeAction()
        } catch (e: Exception) {
            // TODO https://github.com/Kotlin/kotlinx.coroutines/issues/933
            val rootCause = Throwables.getRootCause(e)
            if (rootCause is OAuthNotConfiguredException) {
                handleOAuthConfigurationError(rootCause)
            } else {
                throw e
            }
        }
    }

    private suspend fun configureOAuth(cloud: BitbucketCloud, actionDescription: String): Boolean {
        if (userWantsToConfigureOAuth(actionDescription)) {
            try {
                cloudOAuthConfigurer.asyncConfigureOAuth(cloud).join()
                return true
            } catch (e: OAuthCallbackException) {
                log.info("User didn't configure OAuth for $cloud: ${e.message}")
            }
        } else {
            log.info("User refused to configure OAuth for $cloud")
        }
        return false
    }

    private fun userWantsToConfigureOAuth(actionDescription: String): Boolean {
        val userWantsToConfigureOAuth = AtomicReference<Boolean>()
        ApplicationManager.getApplication().invokeAndWait {
            val result = MessageDialogBuilder
                    .yesNo("Authentication required",
                            """|Linky needs to access Bitbucket on your behalf to $actionDescription.
                               |
                               |You will be navigated to Bitbucket in your browser where you can check the details and grant Linky access.
                               |
                               |Do you like to configure OAuth access now?
                            """.trimMargin())
                    .yesText("Configure OAuth")
                    .noText("Cancel")
                    .project(project)
                    .show()
            userWantsToConfigureOAuth.set(result == Messages.YES)
        }
        return userWantsToConfigureOAuth.get()
    }
}
