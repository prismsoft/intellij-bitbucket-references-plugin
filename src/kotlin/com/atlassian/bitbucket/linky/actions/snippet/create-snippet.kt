package com.atlassian.bitbucket.linky.actions.snippet

import com.atlassian.bitbucket.cloud.rest.snippet.Access.PRIVATE
import com.atlassian.bitbucket.cloud.rest.snippet.Access.PUBLIC
import com.atlassian.bitbucket.linky.actions.Link
import com.atlassian.bitbucket.linky.actions.authenticated.CloudAuthenticator
import com.atlassian.bitbucket.linky.actions.openUriInBrowser
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.SnippetCreate
import com.atlassian.bitbucket.linky.stats.UsageLog
import com.atlassian.bitbucket.linky.stats.cloudEventAttributes
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileTypes.FileTypeManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.openapi.vcs.changes.ChangeListManager
import com.intellij.openapi.vfs.VirtualFile
import kotlinx.coroutines.runBlocking


class CreateSnippetAction : DumbAwareAction("Create Snippet...", "Create Bitbucket Snippet", SnippetCreate) {

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = snippetCreationContext(e) != null
    }

    override fun actionPerformed(e: AnActionEvent) {
        val context = snippetCreationContext(e) ?: return
        val project = context.project

        val cloudRegistry = service<BitbucketCloudRegistry>()

        val dialog = CreateSnippetDialog(project, cloudRegistry.isStagingDiscovered())
        if (dialog.showAndGet()) {
            val title = dialog.getSnippetTitle()
            val cloud = dialog.getBitbucketInstance()
            val scm = dialog.getScm()
            val isPrivate = dialog.isPrivate()

            object : Task.Backgroundable(project, "Creating Snippet...") {
                override fun run(indicator: ProgressIndicator) {
                    val cloudAuthHelper = project.service<CloudAuthenticator>()
                    runBlocking {
                        cloudAuthHelper.run(cloud, "create snippet") {
                            val createSnippet = it.snippets()
                                    .create()
                                    .withAccess(if (isPrivate) PRIVATE else PUBLIC)
                                    .withScm(scm)
                            title?.let { createSnippet.withTitle(it) }

                            with(context) {
                                ReadAction.run<Exception> {
                                    when {
                                        editor != null -> {
                                            val selectedText = selectedTextSnippet(editor)
                                            createSnippet.withFile(file?.name ?: "file") {
                                                selectedText.byteInputStream()
                                            }
                                        }
                                        files.isNotEmpty() -> files
                                                .flatMap { f -> f.snippetHunks(project, "") }
                                                .forEach { (name, file) ->
                                                    createSnippet.withFile(name) { file.inputStream }
                                                }
                                        file != null -> createSnippet.withFile(file.name) { file.inputStream }
                                    }
                                }
                            }
                            runBlocking {
                                val snippet = createSnippet.save()
                                snippet.links["html"]?.href?.let {
                                    openUriInBrowser(project, Link(it, "Snippet"))
                                    service<UsageLog>().event("CreateSnippet", e.place, cloudEventAttributes)
                                }
                            }
                        }
                    }
                }
            }.queue()
        }
    }

    private fun snippetCreationContext(e: AnActionEvent): SnippetCreationContext? {
        val context = e.dataContext

        val project = CommonDataKeys.PROJECT.getData(context)
        val editor = CommonDataKeys.EDITOR.getData(context)
        val file = CommonDataKeys.VIRTUAL_FILE.getData(context)
        val files = CommonDataKeys.VIRTUAL_FILE_ARRAY.getData(context) ?: arrayOf()

        return if (project == null ||
                (editor == null && file == null && files.isEmpty()) ||
                (editor != null && editor.document.textLength == 0)) {
            null
        } else {
            SnippetCreationContext(project, editor, file, files.toList())
        }
    }

    private fun selectedTextSnippet(editor: Editor): String {
        val hunks = with(editor.selectionModel) {
            blockSelectionStarts
                    .zip(blockSelectionEnds)
                    .filter { (start, end) -> start != end }
                    .map { (start, end) -> editor.document.getText(TextRange.create(start, end)) }
        }
        return when {
            hunks.isEmpty() -> editor.document.text
            else -> hunks.joinToString("\n\n...\n\n")
        }
    }

    private fun VirtualFile.snippetHunks(project: Project, namePrefix: String): List<Pair<String, VirtualFile>> =
            if (isDirectory) {
                children
                        .filterNot { isFileIgnored(project, it) }
                        .flatMap { it.snippetHunks(project, "$namePrefix$name/") }
            } else {
                listOf("$namePrefix$name" to this)
            }

    private fun isFileIgnored(project: Project, file: VirtualFile) =
            ChangeListManager.getInstance(project).isIgnoredFile(file) ||
                    FileTypeManager.getInstance().isFileIgnored(file)

    private data class SnippetCreationContext(val project: Project,
                                              val editor: Editor?,
                                              val file: VirtualFile?,
                                              val files: List<VirtualFile>)
}


