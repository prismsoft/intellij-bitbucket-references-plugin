package com.atlassian.bitbucket.linky.actions.snippet

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.snippet.Scm
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.components.JBTextField
import java.awt.FlowLayout
import javax.swing.*
import javax.swing.border.EmptyBorder

class CreateSnippetDialog(project: Project, allowBbInstanceSelection: Boolean = false) : DialogWrapper(project, true) {
    private val titleTextField = JBTextField().apply {
        columns = 30
        emptyText.text = "Snippet title"
        setResizable(false)
    }
    private val privateCheckbox = JCheckBox("Private")
    private val titleAccessPanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.X_AXIS)
        add(titleTextField)
        add(privateCheckbox)
    }

    private val productionRadioButton = JRadioButton("Production").apply {
        border = EmptyBorder(1, 1, 1, 1)
    }
    private val stagingRadioButton = JRadioButton("Staging").apply {
        border = EmptyBorder(1, 1, 1, 1)
    }
    private val bbInstanceButtonGroup = ButtonGroup().apply {
        add(productionRadioButton)
        add(stagingRadioButton)
    }
    private val bbInstancePanel = JPanel().apply {
        layout = FlowLayout()
        add(JLabel("Bitbucket: "))
        bbInstanceButtonGroup.elements.asSequence().forEach { add(it) }
    }

    private val gitRadioButton = JRadioButton("Git").apply {
        border = EmptyBorder(1, 1, 1, 1)
    }
    private val hgRadioButton = JRadioButton("Mercurial").apply {
        border = EmptyBorder(1, 1, 1, 1)
    }
    private val scmButtonGroup = ButtonGroup().apply {
        add(gitRadioButton)
        add(hgRadioButton)
    }
    private val scmPanel = JPanel().apply {
        layout = FlowLayout()
        add(JLabel("Repository type: "))
        scmButtonGroup.elements.asSequence().forEach { add(it) }
    }

    private val mainPanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.Y_AXIS)
        add(titleAccessPanel)
        add(bbInstancePanel)
        add(scmPanel)
    }

    init {
        productionRadioButton.isSelected = true
        if (!allowBbInstanceSelection) {
            bbInstancePanel.isEnabled = false
            bbInstancePanel.isVisible = false
        }
        gitRadioButton.isSelected = true
        privateCheckbox.isSelected = true

        title = "Create Snippet"
        setResizable(false)
        super.init()
    }

    override fun createCenterPanel() = mainPanel

    override fun getPreferredFocusedComponent() = titleTextField

    fun getSnippetTitle(): String? = titleTextField.text

    fun getBitbucketInstance() = when {
        productionRadioButton.isSelected -> BitbucketCloud.Production
        else -> BitbucketCloud.Staging
    }

    fun isPrivate() = privateCheckbox.isSelected

    fun getScm() = if (gitRadioButton.isSelected) Scm.GIT else Scm.HG
}
