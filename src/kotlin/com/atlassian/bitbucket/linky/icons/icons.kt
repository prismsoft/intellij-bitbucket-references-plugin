package com.atlassian.bitbucket.linky.icons

import com.intellij.openapi.util.IconLoader.getIcon

object BitbucketLinkyIcons {

    val Bitbucket = getIcon("/icons/bitbucket.png")

    object Actions {
        val FileCopyUrl = getIcon("/icons/file-copy-link.png")
        val FileOpen = getIcon("/icons/file-open.png")

        val CommitCopyUrl = getIcon("/icons/commit-copy-link.png")
        val CommitOpen = getIcon("/icons/commit-open.png")

        val PullRequestCopyUrl = getIcon("/icons/pr-copy-link.png")
        val PullRequestOpen = getIcon("/icons/pr-open.png")
        val PullRequestFind = getIcon("/icons/pr-find.png")
        val PullRequestCreate = getIcon("/icons/pr-create.png")

        val SnippetCreate = getIcon("/icons/snippet-create.png")
    }

    object Settings {
        val UnknownRepository = getIcon("/icons/repo-unsupported.png")
        val LinkableRepository = getIcon("/icons/repo-unlinked.png")
    }
}
