package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.repository.RepositorySettings
import com.atlassian.bitbucket.linky.repository.SettingKey
import com.atlassian.bitbucket.linky.repository.getOrderedRemoteUrls
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.ProjectComponent
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.ProjectLevelVcsManager
import com.intellij.openapi.vcs.VcsListener
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import java.time.Duration
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

interface BitbucketRepositoriesService {
    fun getBitbucketRepositories(repository: Repository): Map<RemoteUrl, BitbucketRepository>
    fun getBitbucketRepository(repository: Repository): BitbucketRepository?

    fun configurationChanged(awaitTimeout: Duration = Duration.ZERO)
}

class DefaultBitbucketRepositoriesService(private val project: Project,
                                          private val discoveryService: BitbucketRepositoriesDiscoveryService,
                                          private val repositorySettings: RepositorySettings) :
        ProjectComponent, BitbucketRepositoriesService, VcsListener {
    private val discoveryDelay = Duration.ofMinutes(10)
    private val repositories: MutableMap<Repository, MutableMap<RemoteUrl, BitbucketRepository>> = ConcurrentHashMap()
    private val discoveryExecutor = Executors.newSingleThreadScheduledExecutor(
            ThreadFactoryBuilder()
                    .setNameFormat("bitbucket-reference-plugin-remote-discoverer-%d")
                    .setDaemon(true)
                    .build())
    private val discoveryFinished = AtomicInteger(0)
    private val discoveryTask = {
        runBlocking {
            val bbRepos = discoveryService.discoverBitbucketRepositories()
            discoveryFinished.incrementAndGet()
            bbRepos.forEach { (repository, remoteUrl, bitbucketRepository) ->
                repositories
                        .getOrPut(repository) { ConcurrentHashMap() }
                        .putIfAbsent(remoteUrl, bitbucketRepository)
            }
        }
    }

    private var discoveryTaskFuture: Future<Any>? = null

    override fun initComponent() {
        project.messageBus.connect()
                .subscribe(ProjectLevelVcsManager.VCS_CONFIGURATION_CHANGED, this)
    }

    override fun getBitbucketRepositories(repository: Repository): Map<RemoteUrl, BitbucketRepository> {
        return repositories[repository] ?: emptyMap()
    }

    override fun getBitbucketRepository(repository: Repository) =
            getBitbucketRepositories(repository).let { bbRepos ->
                repository.remoteUrlCandidates()
                        .find { bbRepos.keys.contains(it) }
                        ?.let { bbRepos[it] }
            }

    override fun configurationChanged(awaitTimeout: Duration) {
        discoveryTaskFuture?.cancel(true)
        repositories.clear()
        val seenFinishedCount = discoveryFinished.get()
        discoveryExecutor.scheduleAtFixedRate(discoveryTask, 0, discoveryDelay.toMinutes(), TimeUnit.MINUTES)
        if (awaitTimeout > Duration.ZERO) {
            runBlocking {
                withTimeout(awaitTimeout.toMillis()) {
                    while (true) {
                        if (discoveryFinished.get() > seenFinishedCount) {
                            break
                        }
                        delay(100)
                    }
                }
            }
        }
    }

    override fun directoryMappingChanged() {
        configurationChanged()
    }

    private fun Repository.remoteUrlCandidates(): List<RemoteUrl> =
            // if specific remote URL is defined in properties, use it
            repositorySettings.getProperty(this, SettingKey.ALWAYS_LINK_TO_SELECTED_REMOTE)
                    // fallback to remote URL selected based on the repository
                    ?.let { serializedRemote -> RemoteUrl.parse(serializedRemote) }
                    ?.let { listOf(it) }
            // same for the case when no specific remote URL is defined
                    ?: getOrderedRemoteUrls()
}
