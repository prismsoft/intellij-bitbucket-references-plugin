package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.logger
import io.ktor.http.URLBuilder

private val log = logger()

interface BitbucketCloudProbe {
    fun probeBitbucketCloud(detectedScheme: UriScheme,
                            detectedHost: String,
                            detectedPort: Int): BitbucketCloud?
}

class DefaultBitbucketCloudProbe : BitbucketCloudProbe {
    override fun probeBitbucketCloud(detectedScheme: UriScheme,
                                     detectedHost: String,
                                     detectedPort: Int): BitbucketCloud? {
        val url = URLBuilder().apply {
            this.protocol = detectedScheme.toUrlProtocol()
            this.host = detectedHost
            if (port > 0) {
                this.port = detectedPort
            }
        }.build()

        return try {
            BitbucketCloud.forUrl(url)
        } catch (e: IllegalArgumentException) {
            log.debug("Candidate URL doesn't look like Bitbucket Cloud '$url'", e)
            null
        }
    }
}
