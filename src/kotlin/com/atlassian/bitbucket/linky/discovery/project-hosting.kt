package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.HTTP
import com.atlassian.bitbucket.linky.UriScheme.HTTPS
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.repository.ProjectRepositoriesTraverser
import com.atlassian.bitbucket.linky.repository.getRemoteUrls
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.intellij.dvcs.repo.Repository
import kotlinx.coroutines.coroutineScope
import java.net.URI
import java.util.regex.Matcher
import java.util.regex.Pattern

private val log = logger()

interface BitbucketRepositoriesDiscoveryService {
    suspend fun discoverBitbucketRepositories(): List<RepositoryHostedOnBitbucket>
}

data class RepositoryHostedOnBitbucket(val repository: Repository,
                                       val remoteUrl: RemoteUrl,
                                       val bitbucketRepository: BitbucketRepository)

data class RemoteUrl(val scheme: UriScheme, val hostname: String, val port: Int, val path: String) {
    init {
        if (!path.startsWith("/")) {
            throw IllegalArgumentException("Path '$path' should start with a slash")
        }
        if (path.length > 1 && path.endsWith("/")) {
            throw IllegalArgumentException("Path '$path' should not end with a slash")
        }
    }

    fun serialize() = URI(scheme.presentation, null, hostname, port, path, null, null).toString()

    companion object {
        fun parse(string: String): RemoteUrl? {
            val url = URI.create(string)
            return UriScheme.forName(url.scheme)
                    ?.let { RemoteUrl(it, url.host, url.port, url.path) }
        }
    }
}

class DefaultBitbucketRepositoriesDiscoveryService(private val projectRepositoriesTraverser: ProjectRepositoriesTraverser,
                                                   private val cloudDiscoverer: CloudBitbucketRepositoryDiscoverer,
                                                   private val serverDiscoverer: ServerBitbucketRepositoryDiscoverer) : BitbucketRepositoriesDiscoveryService {

    override suspend fun discoverBitbucketRepositories(): List<RepositoryHostedOnBitbucket> = coroutineScope {
        projectRepositoriesTraverser.traverseRepositories()
                .flatMap { repo ->
                    repo.getRemoteUrls()
                            .mapNotNull { url ->
                                try {
                                    val bbRepo =
                                            cloudDiscoverer.discover(repo, url)
                                                    ?: serverDiscoverer.discover(repo, url)
                                                    ?: cloudDiscoverer.discover(repo, url, active = true)
                                                    ?: serverDiscoverer.discover(repo, url, active = true)

                                    if (bbRepo != null) {
                                        return@mapNotNull RepositoryHostedOnBitbucket(repo, url, bbRepo)
                                    }
                                } catch (e: RuntimeException) {
                                    log.debug("Error when discovering Bitbucket for remote URL '$url'")
                                }
                                null
                            }
                }
    }
}

interface BitbucketRepositoryDiscoverer {
    suspend fun discover(repository: Repository,
                         remoteUrl: RemoteUrl,
                         active: Boolean = false): BitbucketRepository?

    fun remoteUrlMatchesPattern(remoteUrl: RemoteUrl): Boolean
}

interface BitbucketRepositoryLinkingHelper<T> {
    fun registerManuallyConfigured(remoteUrl: RemoteUrl, linkTo: T)
}

class CloudBitbucketRepositoryDiscoverer(private val cloudRegistry: BitbucketCloudRegistry,
                                         private val cloudProbe: BitbucketCloudProbe)
    : BitbucketRepositoryDiscoverer, BitbucketRepositoryLinkingHelper<BitbucketCloud> {

    private val pathPattern: Pattern = "/([^/]+)/([^/]+)".toPattern()

    override suspend fun discover(repository: Repository,
                                  remoteUrl: RemoteUrl,
                                  active: Boolean): BitbucketRepository? {
        val matcher = pathMatcher(remoteUrl)
        if (matcher.matches()) {
            val (scheme, host, port, path) = remoteUrl
            log.debug("Remote URL path '$path' matches Bitbucket Cloud repository path pattern")
            val cloud = discoverCloudHosting(scheme, host, port, active)
            if (cloud != null) {
                val workspaceId = matcher.group(1)
                val slug = matcher.group(2)
                return BitbucketRepository.Cloud(repository, remoteUrl, cloud, workspaceId, slug)
            }
        }
        return null
    }

    override fun remoteUrlMatchesPattern(remoteUrl: RemoteUrl): Boolean = pathMatcher(remoteUrl).matches()

    override fun registerManuallyConfigured(remoteUrl: RemoteUrl, linkTo: BitbucketCloud) {
        cloudRegistry.add(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, linkTo)
    }

    private fun pathMatcher(remoteUrl: RemoteUrl): Matcher {
        return pathPattern.matcher(remoteUrl.path)
    }

    private fun discoverCloudHosting(scheme: UriScheme, host: String, port: Int, active: Boolean): BitbucketCloud? {
        var cloud = cloudRegistry.lookup(scheme, host, port)
        if (cloud == null && active) {
            cloud = cloudProbe.probeBitbucketCloud(scheme, host, port)
            if (cloud != null) {
                cloudRegistry.add(scheme, host, port, cloud)
            }
        }

        return cloud
    }
}

class ServerBitbucketRepositoryDiscoverer(private val serverRegistry: BitbucketServerRegistry,
                                          private val serverProbe: BitbucketServerProbe)
    : BitbucketRepositoryDiscoverer, BitbucketRepositoryLinkingHelper<BitbucketServer> {

    private val appPathPattern = "(/.+)?"
    private val fullSlugPattern = "([^/]+)/([^/]+)"
    private val httpPathPattern = "$appPathPattern/scm/$fullSlugPattern".toPattern()
    private val sshPathPattern = "$appPathPattern/$fullSlugPattern".toPattern()

    override suspend fun discover(repository: Repository, remoteUrl: RemoteUrl, active: Boolean): BitbucketRepository? =
            coroutineScope {
                val (scheme, host, port, path) = remoteUrl
                val triple = parseRemoteUrlPath(remoteUrl)
                if (triple != null) {
                    val (appPath, projectKey, slug) = triple
                    log.debug("Remote URL path '$path' matches Bitbucket Server repository path pattern")
                    val server = discoverServerHosting(scheme, host, port, appPath, active)
                    if (server != null) {
                        return@coroutineScope BitbucketRepository.Server(repository, remoteUrl, server, projectKey, slug)
                    }
                }
                null
            }

    override fun remoteUrlMatchesPattern(remoteUrl: RemoteUrl): Boolean = pathMatcher(remoteUrl).matches()

    override fun registerManuallyConfigured(remoteUrl: RemoteUrl, linkTo: BitbucketServer) {
        parseRemoteUrlPath(remoteUrl)?.let {
            val appPath = it.first
            serverRegistry.add(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath, linkTo)
        }
    }

    private fun pathMatcher(remoteUrl: RemoteUrl): Matcher {
        val pathPattern = if (remoteUrl.scheme in arrayOf(HTTP, HTTPS)) httpPathPattern else sshPathPattern
        return pathPattern.matcher(remoteUrl.path)
    }

    private fun parseRemoteUrlPath(remoteUrl: RemoteUrl): Triple<String, String, String>? {
        val matcher = pathMatcher(remoteUrl)
        return if (matcher.matches()) {
            Triple(matcher.group(1) ?: "/", matcher.group(2), matcher.group(3))
        } else null
    }

    private suspend fun discoverServerHosting(scheme: UriScheme, host: String, port: Int, path: String, active: Boolean): BitbucketServer? =
            coroutineScope {
                var server = serverRegistry.lookup(scheme, host, port, path)
                if (server == null && active) {
                    server = serverProbe.probeBitbucketServer(scheme, host, port, path)
                    if (server != null) {
                        serverRegistry.add(scheme, host, port, path, server)
                    }
                }

                server
            }
}
