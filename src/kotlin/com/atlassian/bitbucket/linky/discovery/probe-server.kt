package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.GIT
import com.atlassian.bitbucket.linky.UriScheme.SSH
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.BitbucketServerRestClientException
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import io.ktor.http.Url
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.withTimeout
import org.apache.http.ProtocolException
import java.time.Duration
import java.time.Instant
import javax.net.ssl.SSLException

private val log = logger()
private val probeTimeout = Duration.ofSeconds(10)
private val probeRetryTimeout = Duration.ofHours(1)

interface BitbucketServerProbe {
    suspend fun probeBitbucketServer(detectedScheme: UriScheme,
                                     detectedHost: String,
                                     detectedPort: Int,
                                     detectedApplicationPath: String): BitbucketServer?
}

class DefaultBitbucketServerProbe(private val restClientProvider: BitbucketRestClientProvider) : BitbucketServerProbe {

    // URLs to never retry probing. It is not persisted because
    // we can't be sure things don't change in the outer world.
    private val blackList = mutableSetOf<Url>()
    private val probeTimestamps = mutableMapOf<Url, Instant>()

    override suspend fun probeBitbucketServer(detectedScheme: UriScheme,
                                              detectedHost: String,
                                              detectedPort: Int,
                                              detectedApplicationPath: String): BitbucketServer? {
        val appPath = if (detectedApplicationPath.isEmpty()) "/" else detectedApplicationPath
        for (it in candidates(detectedScheme, detectedHost, detectedPort, appPath)) {
            val server = probe(it)
            if (server != null) {
                return server
            }
        }
        return null
    }

    private fun candidates(detectedScheme: UriScheme,
                           detectedHost: String,
                           detectedPort: Int,
                           detectedPath: String): List<Url> {
        val candidateProtocols = when (detectedScheme) {
            SSH, GIT -> listOf(URLProtocol.HTTPS, URLProtocol.HTTP)
            else -> listOf(detectedScheme.toUrlProtocol())
        }

        return candidateProtocols.flatMap { protocol ->
            listOf(-1, detectedPort)
                    .distinct()
                    .map { port ->
                        URLBuilder().apply {
                            this.protocol = protocol
                            this.host = detectedHost
                            if (port > 0) {
                                this.port = port
                            }
                            this.encodedPath = detectedPath
                        }.build()
                    }
        }
    }

    private suspend fun probe(url: Url): BitbucketServer? = when {
        blackList.contains(url) -> null
        probeTimestamps.getOrDefault(url, Instant.ofEpochMilli(0))
                .isAfter(Instant.now().minus(probeRetryTimeout)) -> null
        else -> {
            log.debug("Probing for Bitbucket Server at '$url")
            try {
                val server = BitbucketServer(url)
                probeTimestamps[url] = Instant.now()
                withTimeout(probeTimeout.toMillis()) {
                    restClientProvider.bitbucketServerRestClient(server, false).use {
                        it.testConnectivity().unauthenticatedResource()
                        server
                    }
                }
            } catch (e: BitbucketServerRestClientException) {
                if (e.cause is SSLException || e.cause is ProtocolException) {
                    blackList.add(url)
                }
                log.debug("Didn't find Bitbucket Server at candidate url '$url'", e)
                null
            } catch (e: TimeoutCancellationException) {
                log.debug("Timeout [${probeTimeout.seconds}s] exceeded while trying Bitbucket Server candidate url '$url'")
                null
            } catch (e: Throwable) {
                log.error(e)
                null
            }
        }
    }
}
