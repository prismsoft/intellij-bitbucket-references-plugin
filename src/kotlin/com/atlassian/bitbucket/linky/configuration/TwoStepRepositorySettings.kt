package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.repository.RepositorySettings
import com.atlassian.bitbucket.linky.repository.SettingKey
import com.intellij.dvcs.repo.Repository

class TwoStepRepositorySettings(private val delegate: RepositorySettings) : RepositorySettings {
    private val intermediateAdd: MutableMap<Repository, MutableMap<SettingKey, String>> = mutableMapOf()
    private val intermediateDelete: MutableMap<Repository, MutableSet<SettingKey>> = mutableMapOf()

    override fun getProperty(repository: Repository, key: SettingKey, defaultValue: String?) =
            intermediateAdd[repository]?.get(key) ?: delegate.getProperty(repository, key, defaultValue)

    override fun setProperty(repository: Repository, key: SettingKey, value: String) {
        // remove the key from properties to remove
        intermediateDelete[repository]?.remove(key)

        intermediateAdd
                .getOrPut(repository, { mutableMapOf() })
                .put(key, value)
    }

    override fun removeProperty(repository: Repository, key: SettingKey) {
        // remove the key from properties to add
        intermediateAdd[repository]?.remove(key)

        intermediateDelete
                .getOrPut(repository, { mutableSetOf() })
                .add(key)
    }

    fun isModified(): Boolean {
        return intermediateAdd.isNotEmpty() || intermediateDelete.isNotEmpty()
    }

    fun apply() {
        intermediateAdd.forEach {
            val repo = it.key
            it.value.forEach { delegate.setProperty(repo, it.key, it.value) }
        }
        intermediateAdd.clear()

        intermediateDelete.forEach {
            val repo = it.key
            it.value.forEach { delegate.removeProperty(repo, it) }
        }
        intermediateDelete.clear()
    }

    fun reset() {
        intermediateAdd.clear()
        intermediateDelete.clear()
    }
}
