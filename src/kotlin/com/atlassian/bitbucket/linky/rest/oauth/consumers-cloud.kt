package com.atlassian.bitbucket.linky.rest.oauth

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.BitbucketCloud.Production
import com.atlassian.bitbucket.cloud.rest.BitbucketCloud.Staging
import com.atlassian.bitbucket.cloud.rest.oauth.OAuthConsumer
import com.atlassian.bitbucket.linky.rest.NoOAuthConsumerException
import io.ktor.http.Url

val BitbucketCloud.oAuthConsumer: OAuthConsumer
    get() = when (this) {
        is Production -> OAuthConsumer("WuCc3RmCsHYjWNfFzC", "S6DKhcxYHzPvNTuUkySnJUy7aCDNcW3y")
        is Staging -> OAuthConsumer("BEjqbADGxN7eYQHSVA", "c75QVcfGYduPnnp4tzyjJtXPrAubcSwW")
        else -> throw NoOAuthConsumerException("OAuth is not available with Bitbucket Cloud ($baseUrl)")
    }

fun BitbucketCloud.oAuthAuthorizationUrl(stateToken: String): Url =
        when (this) {
            is Production ->
                Url("https://bitbucket.org/site/oauth2/authorize?client_id=${oAuthConsumer.id}&response_type=code&state=$stateToken")
            is Staging ->
                Url("https://staging.bb-inf.net/site/oauth2/authorize?client_id=${oAuthConsumer.id}&response_type=code&state=$stateToken")
            else -> throw NoOAuthConsumerException("OAuth is not available with Bitbucket Cloud ($baseUrl)")
        }
