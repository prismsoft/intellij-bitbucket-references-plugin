package com.atlassian.bitbucket.linky.rest.oauth

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.oauth.AuthorizationCode
import com.atlassian.bitbucket.linky.coroutines.Background
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.intellij.ide.BrowserUtil
import io.ktor.http.toURI
import kotlinx.coroutines.*
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.handler.AbstractHandler
import java.time.Duration
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.concurrent.withLock
import kotlin.coroutines.CoroutineContext

private val log = logger()

private const val SERVER_PORT = 23945
private val AUTHORIZATION_TIMEOUT = Duration.ofMinutes(20)

interface BitbucketCloudOAuthConfigurer {
    fun asyncConfigureOAuth(cloud: BitbucketCloud): Job
}

class DefaultBitbucketCloudOAuthConfigurer(private val restClientProvider: BitbucketRestClientProvider)
    : CoroutineScope, BitbucketCloudOAuthConfigurer {

    override val coroutineContext: CoroutineContext get() = Background

    override fun asyncConfigureOAuth(cloud: BitbucketCloud) = launch {
        val stateToken = UUID.randomUUID().toString()
        val url = cloud.oAuthAuthorizationUrl(stateToken)

        val authorizationCode = withTimeout(AUTHORIZATION_TIMEOUT.toMillis()) {
            val deferred = OAuthCallbackListener.authorizationCode(stateToken)
            BrowserUtil.browse(url.toURI())
            deferred.await()
        }

        restClientProvider.bitbucketCloudOAuthClient(cloud).use {
            it.requestOAuthTokens(authorizationCode)
        }
    }
}

object OAuthCallbackListener {
    private val expectCallbackTimeout = Duration.ofMinutes(20)
    private val expectedCallbacks = ConcurrentHashMap<String, CompletableDeferred<AuthorizationCode>>()

    private val serverLifecycleLock = ReentrantLock()
    private val server = Server(SERVER_PORT)
    private val cleanupExecutor = Executors.newSingleThreadScheduledExecutor(
            ThreadFactoryBuilder().setNameFormat("oauth-callback-cleanup-%d").setDaemon(true).build())

    init {
        server.handler = object : AbstractHandler() {
            override fun handle(target: String,
                                baseRequest: Request,
                                request: HttpServletRequest,
                                response: HttpServletResponse) {
                baseRequest.isHandled = true

                if (target == "/linky/oauth") {
                    val parameters = baseRequest.parameterMap
                    val stateToken = parameters["state"]?.firstOrNull()
                            ?: return response.replyWith(HttpServletResponse.SC_BAD_REQUEST, "bad_request.html")
                                    .also { log.info("Query parameter 'state' is required in OAuth callback request: ${baseRequest.describe()}") }

                    val error = parameters["error"]?.firstOrNull()
                    if (error != null) {
                        if (error == "access_denied") {
                            if (authorizationDeclined(stateToken)) {
                                response.replyWith(HttpServletResponse.SC_OK, "authorization_declined.html")
                            } else {
                                return response.replyWith(HttpServletResponse.SC_BAD_REQUEST, "bad_request.html")
                            }
                        } else {
                            val errorDescription = parameters["error_description"]?.firstOrNull()
                            if (authorizationFailed(stateToken, errorDescription ?: error)) {
                                response.replyWith(HttpServletResponse.SC_OK, "authorization_failed.html")
                            } else {
                                return response.replyWith(HttpServletResponse.SC_BAD_REQUEST, "bad_request.html")
                            }
                        }
                    } else {
                        val code = parameters["code"]?.firstOrNull()
                                ?: return response.replyWith(HttpServletResponse.SC_BAD_REQUEST, "bad_request.html")
                                        .also { log.info("Query parameter 'code' is required in OAuth callback request: ${baseRequest.describe()}") }

                        if (authorizationGranted(stateToken, code)) {
                            response.replyWith(HttpServletResponse.SC_OK, "success.html")
                        } else {
                            return response.replyWith(HttpServletResponse.SC_BAD_REQUEST, "bad_request.html")
                        }
                    }

                    cleanupExecutor.schedule({ shutDownServerIfRequired() }, 10, TimeUnit.SECONDS)
                } else {
                    return response.replyWith(HttpServletResponse.SC_NOT_FOUND, "not_found.html")
                            .also { log.warn("Unrecognized HTTP request: '${baseRequest.method} ${baseRequest.httpURI}'") }
                }
            }
        }
    }

    fun authorizationCode(stateToken: String): CompletableDeferred<AuthorizationCode> {
        val deferredAuthorizationCode = CompletableDeferred<AuthorizationCode>()
        val existingDeferred = expectedCallbacks.putIfAbsent(stateToken, deferredAuthorizationCode)
        if (existingDeferred != null) {
            throw IllegalStateException("Already expecting callback for state token '$stateToken'")
        }

        cleanupExecutor.schedule({ timeoutExpectingCallback(stateToken) }, expectCallbackTimeout.toMillis(), TimeUnit.MILLISECONDS)
        startUpServerIfRequired()

        return deferredAuthorizationCode
    }

    private fun authorizationDeclined(stateToken: String): Boolean {
        val publishSubject = publishSubjectForCallback(stateToken)
        publishSubject?.completeExceptionally(OAuthDeclinedException(stateToken))
        return publishSubject != null
    }

    private fun authorizationFailed(stateToken: String, errorMessage: String): Boolean {
        val publishSubject = publishSubjectForCallback(stateToken)
        publishSubject?.completeExceptionally(OAuthCallbackException("OAuth authorization failed for state token $stateToken: $errorMessage"))
        return publishSubject != null
    }

    private fun authorizationGranted(stateToken: String, code: AuthorizationCode): Boolean {
        val publishSubject = publishSubjectForCallback(stateToken)
        publishSubject?.let {
            log.debug("Received OAuth callback for state token '$stateToken'")
            it.complete(code)
        }
        return publishSubject != null
    }

    private fun publishSubjectForCallback(stateToken: String): CompletableDeferred<AuthorizationCode>? {
        val publishSubject = expectedCallbacks.remove(stateToken)
        if (publishSubject == null) {
            log.info("Received unexpected OAuth callback for state token '$stateToken'")
        }
        return publishSubject
    }

    private fun timeoutExpectingCallback(stateToken: String) {
        expectedCallbacks.remove(stateToken)
                ?.completeExceptionally(TimeoutExpectingOAuthCallbackException(stateToken, expectCallbackTimeout))
        shutDownServerIfRequired()
    }

    private fun startUpServerIfRequired() {
        serverLifecycleLock.withLock {
            if (server.isRunning.not()) {
                log.info("Starting OAuth callback listening HTTP server on port $SERVER_PORT")
                try {
                    server.start()
                } catch (e: RuntimeException) {
                    throw IllegalStateException("Failed to start OAuth callback listening HTTP server on port $SERVER_PORT", e)
                }
            }
        }
    }

    private fun shutDownServerIfRequired() {
        serverLifecycleLock.withLock {
            if (expectedCallbacks.isEmpty()) {
                try {
                    server.stop()
                } catch (e: RuntimeException) {
                    log.error("Failed to stop OAuth callback listening HTTP server", e)
                }
            }
        }
    }

    private fun Request.describe() = "$method $httpURI"

    private fun HttpServletResponse.replyWith(status: Int, template: String) {
        this.contentType = "text/html; charset=utf-8"
        this.status = status
        javaClass.classLoader
                .getResourceAsStream("/html/oauth/$template")
                .copyTo(this.outputStream)
    }
}

open class OAuthCallbackException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

class TimeoutExpectingOAuthCallbackException(stateToken: String, timeout: Duration) :
        OAuthCallbackException("OAuth callback for state token '$stateToken' has not been received within ${timeout.toMillis()} ms")

class OAuthDeclinedException(stateToken: String) : OAuthCallbackException("User declined Bitbucket OAuth authorization for state token '$stateToken'")
