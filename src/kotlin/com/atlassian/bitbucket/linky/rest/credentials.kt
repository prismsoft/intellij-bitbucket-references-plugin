package com.atlassian.bitbucket.linky.rest

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.linky.hosting.baseUri
import com.atlassian.bitbucket.linky.hosting.name
import com.atlassian.bitbucket.linky.hosting.uuid
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.intellij.credentialStore.CredentialAttributes

private const val SERVICE_NAME_PREFIX = "IntelliJ Platform"

internal fun credentialsAttributes(cloud: BitbucketCloud): CredentialAttributes {
    val name = cloud.name
    return CredentialAttributes(generateServiceName("Linky", "Bitbucket Cloud $name"), name, null, false)
}

internal fun credentialsAttributes(server: BitbucketServer): CredentialAttributes {
    val name = server.uuid.toString()
    return CredentialAttributes(generateServiceName("Linky", "Bitbucket Server $name"), name, null, false)
}

// Legacy place where token used to live
internal fun refreshTokenCredentialAttributes(cloud: BitbucketCloud) =
        CredentialAttributes(generateServiceName("Linky", "refresh token - ${cloud.baseUri}"), null, null, false)

private fun generateServiceName(subsystem: String, key: String): String = "$SERVICE_NAME_PREFIX $subsystem — $key"
