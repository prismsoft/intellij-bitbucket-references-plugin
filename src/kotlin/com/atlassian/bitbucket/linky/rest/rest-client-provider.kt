package com.atlassian.bitbucket.linky.rest

import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.cloud.rest.BitbucketCloudApi
import com.atlassian.bitbucket.cloud.rest.oauth.*
import com.atlassian.bitbucket.linky.hosting.name
import com.atlassian.bitbucket.linky.rest.oauth.oAuthConsumer
import com.atlassian.bitbucket.linky.utils.createHttpClient
import com.atlassian.bitbucket.server.rest.BitbucketServer
import com.atlassian.bitbucket.server.rest.BitbucketServerApi
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import java.time.Instant

private typealias CloudGuest = com.atlassian.bitbucket.cloud.rest.Authentication.Guest
private typealias CloudOAuth = com.atlassian.bitbucket.cloud.rest.Authentication.OAuth
private typealias ServerGuest = com.atlassian.bitbucket.server.rest.Authentication.Guest
private typealias ServerBearer = com.atlassian.bitbucket.server.rest.Authentication.Bearer

interface BitbucketRestClientProvider {
    fun bitbucketCloudOAuthClient(cloud: BitbucketCloud): BitbucketCloudOAuthApi

    fun bitbucketCloudRestClient(cloud: BitbucketCloud, authenticated: Boolean): BitbucketCloudApi

    fun bitbucketServerRestClient(server: BitbucketServer, authenticated: Boolean): BitbucketServerApi
}

private const val TOKENS_DELIMITER = " : "
private const val ACCESS_TOKEN_EXPIRY_DELIMITER = " @ "

class DefaultBitbucketRestClientProvider(private val passwordSafe: PasswordSafe) : BitbucketRestClientProvider {
    // TODO cache REST clients

    override fun bitbucketCloudOAuthClient(cloud: BitbucketCloud): BitbucketCloudOAuthApi =
            BitbucketCloudOAuthApi {
                instance = cloud
                consumer = cloud.oAuthConsumer
                httpClient { createHttpClient(cloud.baseUrl) }
                onOAuthTokensUpdate = { saveOAuthTokens(cloud, it) }
            }

    override fun bitbucketCloudRestClient(cloud: BitbucketCloud, authenticated: Boolean): BitbucketCloudApi {
        val auth = if (authenticated) {
            val tokens = getOAuthTokens(cloud)
                    ?: throw NoCredentialsException("No OAuth refresh key for Bitbucket Cloud ($cloud)")
            CloudOAuth(cloud.oAuthConsumer, tokens) { saveOAuthTokens(cloud, it) }
        } else {
            CloudGuest
        }

        return BitbucketCloudApi {
            instance = cloud
            authentication = auth
            httpClient { createHttpClient(cloud.apiBaseUrl) }
        }
    }

    override fun bitbucketServerRestClient(server: BitbucketServer, authenticated: Boolean): BitbucketServerApi {
        val auth = if (authenticated) {
            val personalAccessToken = getPersonalAccessToken(server)
                    ?: throw NoCredentialsException("No Personal access token for Bitbucket Server (${server.baseUrl})")
            ServerBearer(personalAccessToken.value)
        } else {
            ServerGuest
        }

        return BitbucketServerApi {
            instance = server
            authentication = auth
            httpClient { createHttpClient(server.apiBaseUrl) }
        }
    }

    private fun getOAuthTokens(cloud: BitbucketCloud): OAuthTokens? {
        // Try to read from the new location
        val tokens = passwordSafe.getPassword(credentialsAttributes(cloud))
        return if (tokens == null) {
            // Try to read from the legacy location, in this case only read
            // refresh token since access token has very likely expired
            passwordSafe.getPassword(refreshTokenCredentialAttributes(cloud))
                    ?.let { RefreshTokenOnly(it) }
        } else {
            if (tokens.contains(TOKENS_DELIMITER)) {
                val refreshToken = tokens.substringBefore(TOKENS_DELIMITER)
                val accessTokenWithExpiry = tokens.substringAfter(TOKENS_DELIMITER)
                val accessToken = accessTokenWithExpiry.substringBefore(ACCESS_TOKEN_EXPIRY_DELIMITER)
                val expiry = Instant.ofEpochMilli(accessTokenWithExpiry.substringAfter(ACCESS_TOKEN_EXPIRY_DELIMITER).toLong())
                RefreshAndAccessTokens(refreshToken, AccessToken(accessToken, expiry))
            } else {
                RefreshTokenOnly(tokens)
            }
        }
    }

    private fun saveOAuthTokens(cloud: BitbucketCloud, tokens: RefreshAndAccessTokens) {
        val (refreshToken, accessToken) = tokens
        val accessTokenWithExpiry =
                "${accessToken.value}$ACCESS_TOKEN_EXPIRY_DELIMITER${accessToken.expiresAt.toEpochMilli()}"
        passwordSafe.set(credentialsAttributes(cloud),
                Credentials(cloud.name, "$refreshToken$TOKENS_DELIMITER$accessTokenWithExpiry"))
    }

    private fun getPersonalAccessToken(server: BitbucketServer): PersonalAccessToken? =
            passwordSafe.getPassword(credentialsAttributes(server))
                    ?.let { PersonalAccessToken(it) }
}

internal data class PersonalAccessToken(val value: String)

open class BitbucketAuthenticationException(message: String) : RuntimeException(message)
class NoCredentialsException(message: String) : BitbucketAuthenticationException(message)
class NoOAuthConsumerException(message: String) : BitbucketAuthenticationException(message)
