package com.atlassian.bitbucket.linky.blame

import com.atlassian.bitbucket.linky.LinkyFile
import com.atlassian.bitbucket.linky.RelativePath
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.repository.isGit
import com.atlassian.bitbucket.linky.repository.isHg
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler
import git4idea.util.StringScanner
import org.zmlx.hg4idea.execution.HgCommandExecutor


private val log = logger()

interface LineBlamer {

    /**
     * Calculates a path to the file and a line number that together correspond to the requested line
     * of current version of the file.
     * Practically, does {@code blame} for a specific line and gets resulting file (might differ from
     * the requested if that file has been renamed later than the line was changed last time)
     * and number of the line that corresponds to the requested one.
     */
    fun blameLine(linkyFile: LinkyFile, lineNumber: Int): Pair<RelativePath, Int>? = null
}

class GitLineBlamer(private val project: Project) : LineBlamer {

    private val git by lazy { service<Git>() }

    override fun blameLine(linkyFile: LinkyFile, lineNumber: Int): Pair<RelativePath, Int>? {
        val blameCommand = createBlameCommand(linkyFile, lineNumber)
        log.debug("Running command '$this'")
        val result = git.runCommand(blameCommand)

        return if (result.success()) {
            parseReferenceFromBlameOutput(result.outputAsJoinedString)
        } else {
            log.error("Error running command '$this': ${result.errorOutputAsJoinedString}")
            null
        }
    }

    private fun createBlameCommand(linkyFile: LinkyFile, lineNumber: Int): GitLineHandler {
        val handler = GitLineHandler(project, linkyFile.repository.root, GitCommand.BLAME)
        handler.setStderrSuppressed(true)
        handler.charset = linkyFile.virtualFile.charset
        // TODO 2018.3 GitVcsApplicationSettings.getInstance().getAnnotateDetectMovementsOption() == INNER -> -M, == OUTER => -C, otherwise nothing
        // git blame -p -L51,+1 -n -w revision_number -- path/to/the/virtualFile.txt
        handler.addParameters("--porcelain", "-L$lineNumber,+1", "-n", "-w", linkyFile.revision)
        handler.endOptions()
        handler.addParameters(linkyFile.relativePath)
        return handler
    }

    private fun parseReferenceFromBlameOutput(output: String): Pair<RelativePath, Int>? {
        val s = StringScanner(output)
        s.spaceToken() // skip commit hash
        val lineNumber = s.spaceToken().toIntOrNull()

        if (lineNumber == null) {
            log.error("Failed to parse line number from git blame output '$output'")
            return null
        }

        s.nextLine()

        var filePath: String? = null
        while (s.hasMoreData() && !s.startsWith('\t')) {
            val key = s.spaceToken()
            val value = s.line()
            if (key == "filename") {
                filePath = value
                break
            }
        }

        return filePath?.let { Pair(it, lineNumber) }
    }
}

class HgLineBlamer(private val project: Project) : LineBlamer {
    private val linePattern = "\\s*[0-9a-fA-F]+\\s+(.+):\\s*([0-9]+):\\s.*".toPattern()

    override fun blameLine(linkyFile: LinkyFile, lineNumber: Int) =
            runAnnotateCommand(linkyFile)
                    // index starts from 0, line number - from 1
                    .getOrNull(lineNumber - 1)
                    ?.let { parseReferenceFromBlameOutput(it) }

    private fun runAnnotateCommand(linkyFile: LinkyFile): List<String> {
        // hg annotate -f -c -l -w -r revision svnImportScript.sh
        val arguments = listOf("-f", "-c", "-l", "-w", "-r", linkyFile.revision, linkyFile.relativePath)
        log.debug("Running command 'hg annotate $arguments'")

        val result = HgCommandExecutor(project)
                .executeInCurrentThread(linkyFile.repository.root, "annotate", arguments)

        return result?.outputLines ?: emptyList()
    }

    private fun parseReferenceFromBlameOutput(outputLine: String): Pair<RelativePath, Int>? {
        val matcher = linePattern.matcher(outputLine)
        if (matcher.matches()) {
            val lineNumber = matcher.group(2).toIntOrNull()

            if (lineNumber == null) {
                log.error("Failed to parse line number from hg annotate output line '$outputLine'")
                return null
            }

            return Pair(matcher.group(1), lineNumber)
        }

        log.error("Hg output doesn't match expected pattern: '$outputLine'")
        return null
    }
}

fun Repository.blameLineReferenceProvider(): LineBlamer =
        if (isGit()) {
            project.service<GitLineBlamer>()
        } else if (isHg()) {
            project.service<HgLineBlamer>()
        } else {
            object : LineBlamer {}
        }
