package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.repository.RepositorySettings
import com.atlassian.bitbucket.linky.repository.SettingKey
import com.atlassian.bitbucket.linky.selection.LinesSelection
import com.atlassian.bitbucket.linky.selection.compileStringReference
import com.atlassian.bitbucket.util.resolve
import io.ktor.http.URLBuilder
import io.ktor.http.takeFrom
import io.ktor.http.toURI
import java.net.URI
import java.util.*

typealias Revision = String

fun Revision.short() = this.substring(0, Math.min(length, 8))

interface BitbucketLinky {

    /**
     * @return repository this Linky generates links for
     */
    fun getRepository(): BitbucketRepository

    /**
     * @return base [URI] of the repository
     */
    fun getBaseUri(): URI

    /**
     * Builds a [URI] to the requested commit view.
     */
    fun getCommitViewUri(commitReference: Revision): URI

    /**
     * Builds a [URI] to the commit view with the requested line in the file selected.
     * The file that appears in the commit is the ancestor of the requested file,
     * and the selected line historically corresponds to the requested line.
     */
    fun getCommitViewUri(commitReference: Revision, linkyFile: LinkyFile, lineNumber: Int): URI

    /**
     * @return [URI] to the source view of current branch/commit of the repository
     */
    fun getSourceViewUri(): URI

    /**
     * @return [URI] to the source view with selected lines of the specific file
     */
    fun getSourceViewUri(linkyFile: LinkyFile, linesSelections: List<LinesSelection>): URI

    /**
     * Builds URI for pull request for currently selected branch
     *
     * @return {@link Optional} with [URI] to pull request creation page if current branch name is known,
     * {@link Optional#empty()} otherwise
     */
    fun getPullRequestUri(): Optional<URI>
}

class CloudLinky(private val cloud: BitbucketRepository.Cloud,
                 private val settings: RepositorySettings) : BitbucketLinky {
    private val sourceViewRelativePath = "src"

    override fun getRepository(): BitbucketRepository = cloud

    override fun getBaseUri(): URI = cloud.baseUri.toURI()

    override fun getCommitViewUri(commitReference: Revision): URI =
            cloud.baseUri.resolve("commits/$commitReference").toURI()

    override fun getCommitViewUri(commitReference: Revision, linkyFile: LinkyFile, lineNumber: Int): URI =
            URLBuilder().apply {
                takeFrom(cloud.baseUri.resolve("commits/$commitReference"))
                val blame = linkyFile.blameLine(lineNumber)
                if (blame != null) {
                    val (file, line) = blame
                    fragment = "L${file}T$line"
                }
            }.build().toURI()

    override fun getSourceViewUri(): URI = cloud.baseUri.resolve(sourceViewRelativePath).toURI()

    override fun getSourceViewUri(linkyFile: LinkyFile,
                                  linesSelections: List<LinesSelection>): URI =
            URLBuilder().apply {
                takeFrom(cloud.baseUri.resolve("$sourceViewRelativePath/${linkyFile.revision}/${linkyFile.relativePath}"))
                linesSelections.compileStringReference(linkyFile.name + "-", ",", ":", "")
                        ?.let { fragment = it }
            }.build().toURI()

    override fun getPullRequestUri(): Optional<URI> =
            cloud.repository.currentBranchName.let {
                Optional.ofNullable(it)
                        .map { currentBranch ->
                            URLBuilder().apply {
                                takeFrom(cloud.baseUri.resolve("pull-requests/new"))
                                parameters["source"] = currentBranch
                                settings.getProperty(cloud.repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME)?.let { dest ->
                                    parameters["dest"] = dest
                                }
                            }.build().toURI()
                        }
            }
}

class ServerLinky(private val server: BitbucketRepository.Server,
                  private val settings: RepositorySettings) : BitbucketLinky {
    private val sourceViewRelativePath = "browse"

    override fun getRepository(): BitbucketRepository = server

    override fun getBaseUri(): URI = server.baseUri.toURI()

    override fun getCommitViewUri(commitReference: Revision): URI =
            URLBuilder().apply {
                takeFrom(server.baseUri.resolve("commits/$commitReference"))
            }.build().toURI()

    override fun getCommitViewUri(commitReference: Revision, linkyFile: LinkyFile, lineNumber: Int): URI =
            URLBuilder().apply {
                takeFrom(server.baseUri.resolve("commits/$commitReference"))
                val blame = linkyFile.blameLine(lineNumber)
                if (blame != null) {
                    val (file, _) = blame
                    // Bitbucket Server doesn't support line selection in the commit view, so just jump on the file
                    fragment = file
                }
            }.build().toURI()

    override fun getSourceViewUri(): URI = server.baseUri.resolve(sourceViewRelativePath).toURI()

    override fun getSourceViewUri(linkyFile: LinkyFile,
                                  linesSelections: List<LinesSelection>): URI =
            URLBuilder().apply {
                takeFrom(server.baseUri.resolve("$sourceViewRelativePath/${linkyFile.relativePath}"))
                parameters["at"] = linkyFile.revision
                linesSelections.compileStringReference("", ",", "-", "")
                        ?.let { fragment = it }
            }.build().toURI()

    override fun getPullRequestUri(): Optional<URI> =
            server.repository.currentBranchName.let {
                Optional.ofNullable(it)
                        .map { currentBranch ->
                            URLBuilder().apply {
                                takeFrom(server.baseUri.resolve("pull-requests"))
                                // Workaround to set no actual value for `create` query parameter
                                parameters.appendMissing("create", emptyList())
                                parameters["sourceBranch"] = currentBranch
                                settings.getProperty(server.repository, SettingKey.PULL_REQUEST_DEFAULT_TARGET_BRANCH_NAME)?.let { dest ->
                                    parameters["targetBranch"] = dest
                                }
                            }.build().toURI()
                        }
            }
}
