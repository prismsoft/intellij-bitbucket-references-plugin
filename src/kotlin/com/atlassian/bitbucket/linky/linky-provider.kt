package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.atlassian.bitbucket.linky.repository.RepositorySettings
import com.intellij.dvcs.repo.Repository

interface BitbucketLinkyProvider {
    fun getBitbucketLinky(repository: Repository): BitbucketLinky?
}

class DefaultBitbucketLinkyProvider(private val bitbucketRepositoriesService: BitbucketRepositoriesService,
                                    private val repositorySettings: RepositorySettings) : BitbucketLinkyProvider {

    override fun getBitbucketLinky(repository: Repository): BitbucketLinky? =
            bitbucketRepositoriesService.getBitbucketRepository(repository)
                    ?.let { bitbucketRepo ->
                        when (bitbucketRepo) {
                            is BitbucketRepository.Cloud -> CloudLinky(bitbucketRepo, repositorySettings)
                            is BitbucketRepository.Server -> ServerLinky(bitbucketRepo, repositorySettings)
                        }
                    }
}
