package com.atlassian.bitbucket.linky.utils

import com.atlassian.bitbucket.linky.coroutines.Background
import kotlinx.coroutines.withContext

internal suspend fun <P, I> fetchAllPagedItems(firstPage: P,
                                               fetchNextPage: suspend (P) -> P?,
                                               getPageItems: (P) -> List<I>) =
        withContext(Background) {
            fetchAll(mutableListOf(), firstPage, fetchNextPage, getPageItems)
        }

private tailrec suspend fun <P, I> fetchAll(accumulator: MutableList<I>,
                                            page: P?,
                                            fetchNextPage: suspend (P) -> P?,
                                            getPageItems: (P) -> List<I>): MutableList<I> =
        when (page) {
            null -> accumulator
            else -> fetchAll(
                    accumulator.apply { addAll(getPageItems(page)) },
                    fetchNextPage(page),
                    fetchNextPage,
                    getPageItems
            )
        }
