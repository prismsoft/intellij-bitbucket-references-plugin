package com.atlassian.bitbucket.linky.utils

import com.atlassian.bitbucket.linky.logger
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.intellij.util.net.HttpConfigurable
import com.intellij.util.net.ssl.CertificateManager
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logging
import io.ktor.http.Url
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.NTCredentials
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.config.AuthSchemes
import org.apache.http.impl.client.BasicCredentialsProvider

typealias KtorLogger = io.ktor.client.features.logging.Logger

private val log = logger()

fun createHttpClient(baseUrl: Url): HttpClient =
// TODO feature to show warning on 407: HttpStatusCode.ProxyAuthenticationRequired
        HttpClient(Apache) {
            followRedirects = true

            install(Logging) {
                level = LogLevel.HEADERS
                logger = object : KtorLogger {
                    override fun log(message: String) {
                        log.debug(message)
                    }
                }
            }

            engine {
                sslContext = CertificateManager.getInstance().sslContext
                customizeClient {
                    setThreadFactory(ThreadFactoryBuilder()
                            .setNameFormat("bitbucket-linky-http-client-%d")
                            .setDaemon(true)
                            .build())

                    // Setup algorithm copied from com.intellij.util.net.IdeHttpClientHelpers.ApacheHttpClient4
                    val httpConfig = HttpConfigurable.getInstance()
                    if (httpConfig.USE_HTTP_PROXY && httpConfig.isHttpProxyEnabledForUrl(baseUrl.toString())) {
                        val proxyHost = httpConfig.PROXY_HOST ?: ""
                        val proxyPort = httpConfig.PROXY_PORT
                        setProxy(HttpHost(proxyHost, proxyPort))

                        if (httpConfig.PROXY_AUTHENTICATION) {
                            val proxyLogin = httpConfig.proxyLogin ?: ""
                            val proxyPassword = httpConfig.plainProxyPassword ?: ""
                            setDefaultCredentialsProvider(BasicCredentialsProvider().apply {
                                val ntlmUserPassword = "${proxyLogin.replace('\\', '/')}:$proxyPassword"
                                setCredentials(AuthScope(proxyHost, proxyPort, AuthScope.ANY_REALM, AuthSchemes.NTLM),
                                        NTCredentials(ntlmUserPassword))
                                setCredentials(AuthScope(proxyHost, proxyPort), UsernamePasswordCredentials(proxyLogin, proxyPassword))
                            })
                        }
                    }
                }
            }
        }
