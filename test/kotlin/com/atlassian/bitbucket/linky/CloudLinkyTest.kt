package com.atlassian.bitbucket.linky

import assertk.assertThat
import assertk.assertions.hasToString
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import com.atlassian.bitbucket.cloud.rest.BitbucketCloud
import com.atlassian.bitbucket.linky.blame.LineBlamer
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.repository.RepositorySettings
import com.atlassian.bitbucket.linky.selection.LinesSelection
import com.intellij.dvcs.repo.Repository
import com.intellij.mock.MockVirtualFile
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.net.URLEncoder

internal class CloudLinkyTest {

    private val lineBlamer = mockk<LineBlamer>()
    private val repository = mockk<Repository>()
    private val settings = mockk<RepositorySettings>()

    private lateinit var linkyFile: LinkyFile
    private lateinit var linky: CloudLinky

    @BeforeEach
    internal fun setUp() {
        val virtualFile = MockVirtualFile("file")
        linkyFile = LinkyFile(virtualFile, repository, "someRevision", "dir/file", lineBlamer)

        val remoteUrl = RemoteUrl(UriScheme.HTTP, "example.com", 80, "/")

        val cloudRepo = BitbucketRepository.Cloud(
                repository,
                remoteUrl,
                BitbucketCloud.Production,
                "someOwner",
                "someSlug"
        )
        linky = CloudLinky(cloudRepo, settings)

        every { lineBlamer.blameLine(linkyFile, 239) } returns null
        every { settings.getProperty(any(), any()) } returns null
    }

    @Test
    fun `test getCommitViewUri with lines selected`() {
        every { lineBlamer.blameLine(linkyFile, 239) } returns Pair("some/File", 405)

        val commitUri = linky.getCommitViewUri("sha", linkyFile, 239)

        assertThat(commitUri).hasToString("https://bitbucket.org/someOwner/someSlug/commits/sha#Lsome/FileT405")
    }

    @Test
    fun `test getCommitViewUri with SHA reference`() {
        val commitUri = linky.getCommitViewUri("abcdef", linkyFile, 239)

        assertThat(commitUri).hasToString("https://bitbucket.org/someOwner/someSlug/commits/abcdef")
    }

    @Test
    fun `test getSourceViewUri`() {
        val sourceUri = linky.getSourceViewUri()

        assertThat(sourceUri).hasToString("https://bitbucket.org/someOwner/someSlug/src")
    }

    @Test
    fun `test getSourceViewUri for file`() {
        val sourceUri = linky.getSourceViewUri(linkyFile, listOf())

        assertThat(sourceUri).hasToString("https://bitbucket.org/someOwner/someSlug/src/someRevision/dir/file")
    }

    @Test
    fun `test getSourceViewUri for file with lines selected`() {
        val sourceUri = linky.getSourceViewUri(linkyFile, listOf(LinesSelection(1, 7), LinesSelection(239, 405)))

        assertThat(sourceUri).hasToString("https://bitbucket.org/someOwner/someSlug/src/someRevision/dir/file#file-1:7,239:405")
    }

    @Test
    fun `test getPullRequestUri when branch absent`() {
        every { repository.currentBranchName } returns null

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isFalse()
    }

    @Test
    fun `test getPullRequestUri when branch present`() {
        every { repository.currentBranchName } returns "someBranch"

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isTrue()
        assertThat(maybePullRequestUri.get())
                .hasToString("https://bitbucket.org/someOwner/someSlug/pull-requests/new?source=someBranch")
    }

    @Test
    fun `test getPullRequestUri branch name escaped`() {
        val branchName = "branch#5><|.hello!&])({}@.@$&+_-`'\""
        every { repository.currentBranchName } returns branchName
        val encodedBranch = URLEncoder.encode(branchName, Charsets.UTF_8.name())

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isTrue()
        assertThat(maybePullRequestUri.get())
                .hasToString("https://bitbucket.org/someOwner/someSlug/pull-requests/new?source=$encodedBranch")
    }

}
